/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Models;

import fr.csi.Database.POJO.Salarie;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class SalarieModel extends AbstractTableModel {

    private ArrayList<Salarie> listeSalaries = new ArrayList<>();
    private String[] columnsName = new String[]{"Salarié", "Nom & Prénom"};
    
    public SalarieModel(ArrayList<Salarie> _listeSalarie) {
        this.listeSalaries = _listeSalarie;
    }
    
    @Override
    public int getRowCount() {
        return listeSalaries.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Salarie monSalarie = listeSalaries.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return monSalarie.getIdSalarie();
            }
            case 1: {
                return monSalarie.getNomPrenomSalarie();
            }
            default:
                return "";
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex < 0 || columnIndex > (columnsName.length + 1)) {
            return "Cette colonne n'existe pas";
        } else {
            return columnsName[columnIndex];
        }
    }
    
    public void setData(ArrayList<Salarie> _listeSalarie){
        this.listeSalaries=_listeSalarie;
    }

}

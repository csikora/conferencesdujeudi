/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Models;

import fr.csi.Database.DatabaseUtiles;
import static fr.csi.Database.DatabaseUtiles.exec;
//import static fr.csi.Database.DatabaseUtiles.getNomColonnes;
import fr.csi.Database.POJO.Conference;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class ConferenceModel extends AbstractTableModel {

    private ArrayList<Conference> listeConferences = new ArrayList<>();
    private String[] columnsName=new String[]{"Titre","Date","Conférencier","Salle","Thème"};
    
    /*Avant la mise en place du MVC : pour affichage des entêtes de BDD
    String query = "SELECT * FROM Conference";
    ResultSet resultat = exec(query);
    private String[] columnsName =getNomColonnes(resultat);*/

    public ConferenceModel(ArrayList<Conference> _listeConference) {
        this.listeConferences = _listeConference;
    }

    @Override
    public int getRowCount() {
        return listeConferences.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference maConference = listeConferences.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return maConference.getTitreConference();
            }
            case 1: {
                return maConference.getDateString();
            }
            case 2: {
                return maConference.getConferencier().getNomPrenomConferencier();
            }
            case 3: {
                return maConference.getSalle().getNomSalle();
            }
            case 4: {
                return maConference.getTheme().getDesignationTheme();
            }
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex < 0 || columnIndex > (columnsName.length + 1)) {
            return "Cette colonne n'existe pas";
        } else {
            return columnsName[columnIndex];
        }
    }
    
    public void setData(ArrayList<Conference> _listeConference){
        this.listeConferences=_listeConference;
    }
    
    //Récupère la liste des conférences du modèle
    public ArrayList<Conference> getListeConference(){
        return this.listeConferences;
    }

    //Récupère la conférence de la ligne sélectionnée
    public Conference getValue(int rowSelected) {
        return this.listeConferences.get(rowSelected);
    }

}

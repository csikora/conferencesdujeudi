/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Models;

import fr.csi.Database.POJO.Salarie;
import fr.csi.Database.POJO.Salle;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class SalleModel extends AbstractTableModel{
    
    private ArrayList<Salle> listeSalles = new ArrayList<>();
    private String[] columnsName = new String[]{"Salle", "Nom de la salle", "Nombre de places"};
    
    public SalleModel(ArrayList<Salle> _listeSalle) {
        this.listeSalles = _listeSalle;
    }

    @Override
    public int getRowCount() {
        return listeSalles.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Salle maSalle = listeSalles.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return maSalle.getIdSalle();
            }
            case 1: {
                return maSalle.getNomSalle();
            }
            case 2: {
                return maSalle.getNbPlaceSalle();
            }
            default:
                return "";
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex < 0 || columnIndex > (columnsName.length + 1)) {
            return "Cette colonne n'existe pas";
        } else {
            return columnsName[columnIndex];
        }
    }
    
    public void setData(ArrayList<Salle> _listeSalle){
        this.listeSalles=_listeSalle;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Models;

import fr.csi.Database.POJO.Salle;
import fr.csi.Database.POJO.Theme;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class ThemeModel extends AbstractTableModel{
    
    private ArrayList<Theme> listeThemes = new ArrayList<>();
    private String[] columnsName = new String[]{"Theme", "Désignation du thème"};
    
    public ThemeModel(ArrayList<Theme> _listeTheme) {
        this.listeThemes = _listeTheme;
    }

    @Override
    public int getRowCount() {
        return listeThemes.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Theme monTheme = listeThemes.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return monTheme.getIdTheme();
            }
            case 1: {
                return monTheme.getDesignationTheme();
            }
            default:
                return "";
        }
    }
    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex < 0 || columnIndex > (columnsName.length + 1)) {
            return "Cette colonne n'existe pas";
        } else {
            return columnsName[columnIndex];
        }
    }
    
    public void setData(ArrayList<Theme> _listeTheme){
        this.listeThemes=_listeTheme;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Models;

import fr.csi.Database.POJO.Conferencier;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Asus
 */
public class ConferencierModel extends AbstractTableModel {

    private ArrayList<Conferencier> listeConferenciers = new ArrayList<>();
    private String[] columnsName = new String[]{"Conferencier","Nom et prénom", "Interne / Externe"};

    public ConferencierModel(ArrayList<Conferencier> _listeConferencier) {
        this.listeConferenciers = _listeConferencier;
    }

    @Override
    public int getRowCount() {
        return listeConferenciers.size();
    }

    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conferencier monConferencier = listeConferenciers.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return monConferencier.getIdConferencier();
            }
            case 1: {
                return monConferencier.getNomPrenomConferencier();
            }
            case 2: {
                if(monConferencier.isConferencierInterne()){
                    return "Interne";
                } else{
                    return "Externe";
                }
            }
            default:
                return "";
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex < 0 || columnIndex > (columnsName.length + 1)) {
            return "Cette colonne n'existe pas";
        } else {
            return columnsName[columnIndex];
        }
    }
    
    public void setData(ArrayList<Conferencier> _listeConferencier){
        this.listeConferenciers=_listeConferencier;
    }

}

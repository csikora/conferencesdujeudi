/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.ConferenceDAO;
import fr.csi.Database.DAO.ConferencierDAO;
import fr.csi.Database.DAO.SalleDAO;
import fr.csi.Database.DAO.ThemeDAO;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.Conferencier;
import fr.csi.Database.POJO.Salle;
import fr.csi.Database.POJO.Theme;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class AddConferencePanel extends DesignPanel {

    //les labels
    private JLabel labelTitrePage = new JLabel("Ajouter une conférence");
    private JLabel labelTitre = new JLabel("Titre");
    private JLabel labelDate = new JLabel("Date (jj-mm-aaaa)");
    private JLabel labelConferencier = new JLabel("Conférencier");
    private JLabel labelSalle = new JLabel("Salle");
    private JLabel labelTheme = new JLabel("Theme");

    //les champs de saisie
    private JTextField textTitre = new JTextField();
    private DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    private JFormattedTextField textDate = new JFormattedTextField(format);
    private JComboBox comboConferencier;
    private JComboBox comboSalle;
    private JComboBox comboTheme;

    //bouton
    private JButton btnValider = new JButton("Valider");

    public AddConferencePanel() {
        Box mainBox = Box.createVerticalBox();//boite principale
        Box titrePageBox = Box.createHorizontalBox();//boite pour le titre de la page
        Box titreLBox = Box.createVerticalBox();//boite pour le label du titre
        Box titreTBox = Box.createVerticalBox();//boite pour le texte du titre
        Box dateLBox = Box.createVerticalBox();//boite pour le label de la date
        Box dateTBox = Box.createVerticalBox();//boite pour le texte de la date
        Box conferencierLBox = Box.createVerticalBox();//boite pour le label du conférencier
        Box conferencierTBox = Box.createVerticalBox();//boite pour le texte du conférencier
        Box salleLBox = Box.createVerticalBox();//boite pour le label de la salle
        Box salleCBox = Box.createVerticalBox();//boite pour le combo de la salle
        Box themeLBox = Box.createVerticalBox();//boite pour le label du theme
        Box themeCBox = Box.createVerticalBox();//boite pour le combo du theme
        Box titreBox = Box.createHorizontalBox();//boite pour les éléments du titre
        Box dateBox = Box.createHorizontalBox();//boite pour les éléments de la date
        Box conferencierBox = Box.createHorizontalBox();//boite pour les éléments du conférencier
        Box salleBox = Box.createHorizontalBox();//boite pour les éléments de la salle
        Box themeBox = Box.createHorizontalBox();//boite pour les éléments du theme
        Box btnBox = Box.createHorizontalBox();//boite pour le bouton

        //implémentation des combos
        //Grâce aux methodes @Override getString ajoutées aux objets, JComboBox affiche le nom des objets
        ArrayList<Conferencier> maListeConferenciers = new ArrayList<>();
        maListeConferenciers = ConferencierDAO.getAll();
        comboConferencier = new JComboBox(maListeConferenciers.toArray());

        ArrayList<Salle> maListeSalles = new ArrayList<>();
        maListeSalles = SalleDAO.getAll();
        comboSalle = new JComboBox(maListeSalles.toArray());

        ArrayList<Theme> maListeThemes = new ArrayList<>();
        maListeThemes = ThemeDAO.getAll();
        comboTheme = new JComboBox(maListeThemes.toArray());

        //action du bouton ajouter : ajouter la conférence
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterConference();
            }
        });

        //mise en place des différents éléments
        titrePageBox.add(labelTitrePage);
        titreLBox.add(labelTitre);
        dateLBox.add(labelDate);
        conferencierLBox.add(labelConferencier);
        salleLBox.add(labelSalle);
        themeLBox.add(labelTheme);
        titreTBox.add(textTitre);
        dateTBox.add(textDate);
        conferencierTBox.add(comboConferencier);
        salleCBox.add(comboSalle);
        themeCBox.add(comboTheme);
        titreBox.add(titreLBox);
        titreBox.add(titreTBox);
        dateBox.add(dateLBox);
        dateBox.add(dateTBox);
        conferencierBox.add(conferencierLBox);
        conferencierBox.add(conferencierTBox);
        salleBox.add(salleLBox);
        salleBox.add(salleCBox);
        themeBox.add(themeLBox);
        themeBox.add(themeCBox);
        btnBox.add(btnValider);
        mainBox.add(titreBox);
        mainBox.add(titrePageBox);
        mainBox.add(titreBox);
        mainBox.add(dateBox);
        mainBox.add(conferencierBox);
        mainBox.add(salleBox);
        mainBox.add(themeBox);
        mainBox.add(btnBox);

        this.add(mainBox);

        //mise en forme du Titre de la page
        designLabelTitre(labelTitrePage);

        //mise en forme le bouton
        designButton(btnValider);

        //mise en forme des labels
        designLabel(labelTitre);
        designLabel(labelDate);
        designLabel(labelConferencier);
        designLabel(labelSalle);
        designLabel(labelTheme);

        //mise en forme des champs de saisie
        designField(textTitre);
        designDateField(textDate);
        designCombo(comboConferencier);
        designCombo(comboSalle);
        designCombo(comboTheme);
    }

    public void ajouterConference() {
        JOptionPane jop;
        String msg = "";

        //tests de saisie
        if (this.textTitre.getText().isEmpty()) {
            msg = "Vous devez renseigner le titre de la conférence";
        } else if (this.textDate.getText().isEmpty()) {
            msg = "Vous devez renseigner la date de la conférence";
        } else if (comboConferencier.getSelectedItem().equals(null)) {
            msg = "Vous devez renseigner le conférencier de la conférence";
        } else if (comboSalle.getSelectedItem().equals(null)) {
            msg = "Vous devez renseigner la salle de la conférence";
        } else if (comboTheme.getSelectedItem().equals(null)) {
            msg = "Vous devez renseigner le thème de la conférence";
        }

        if (msg != "") {
            jop = new JOptionPane();
            jop.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
        } else {
            String titre = this.textTitre.getText();
            Calendar cal = Calendar.getInstance();
            Date date = null;
            try {
                date = this.format.parse(this.textDate.getText());
            } catch (ParseException ex) {
                Logger.getLogger(AddConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            cal.setTime(date);
            Conferencier conferencier = (Conferencier) comboConferencier.getSelectedItem();
            Salle salle = (Salle) comboSalle.getSelectedItem();
            Theme theme = (Theme) comboTheme.getSelectedItem();
            
            Conference conference = new Conference(titre, cal, conferencier, salle, theme);
            ConferenceDAO.insert(conference);
            //pour mettre à jour la liste des conférences dans la page d'accueil et la page affichant les conf
            Home.monModel.setData(ConferenceDAO.getAllNew());
            Home.monModel.fireTableDataChanged();
            ListConferencePanel.monModel.setData(ConferenceDAO.getAll());
            ListConferencePanel.monModel.fireTableDataChanged();
        }
    }

}

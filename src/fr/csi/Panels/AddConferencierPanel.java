/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.ConferencierDAO;
import fr.csi.Database.DAO.SalarieDAO;
import fr.csi.Database.POJO.Conferencier;
import fr.csi.Database.POJO.Salarie;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class AddConferencierPanel extends DesignPanel {

    //les labels
    private JLabel labelTitrePage = new JLabel("Ajouter un conférencier");
    private JLabel labelConf = new JLabel("Nom et prénom");
    private JLabel labelSalarie = new JLabel("Choisir un salarié");

    //les champs de saisie
    private JTextField textConf = new JTextField();

    //les combos
    private JComboBox comboSalarie;

    //les checkbox
    private JCheckBox checkInterne = new JCheckBox("Conférencier Interne");
    private JCheckBox checkExterne = new JCheckBox("Conférencier Externe");

    //les textarea
    private JTextArea textInterne = new JTextArea(10,10);
    private JTextArea textExterne = new JTextArea(10,10);

    //bouton
    private JButton btnValider = new JButton("Valider");

    public AddConferencierPanel() {
        Box mainBox = Box.createVerticalBox();//boite principale
        Box titrePageBox = Box.createHorizontalBox();//boite pour le titre de la page
        Box confLBox = Box.createVerticalBox();//boite pour le label du nom et prénom
        Box confTBox = Box.createVerticalBox();//boite pour le texte du nom et prénom
        Box salarieLBox = Box.createVerticalBox();//boite pour le label du salarie
        Box salarieTBox = Box.createVerticalBox();//boite pour le combo du salarie
        Box confBox = Box.createHorizontalBox();//boite pour les éléments du conférencier
        Box salarieBox = Box.createHorizontalBox();//boite pour les éléments du salarié
        Box checkIBox = Box.createHorizontalBox();//boite pour la checkBox I
        Box checkEBox = Box.createHorizontalBox();//boite pour la checkBox E
        Box areaIBox = Box.createHorizontalBox();//boite pour la textArea I
        Box areaEBox = Box.createHorizontalBox();//boite pour la textArea E
        Box interneBox = Box.createVerticalBox();//boite pour les éléments du conférencier Interne
        Box externeBox = Box.createVerticalBox();//boite pour les éléments du conférencier Externe
        Box btnBox = Box.createHorizontalBox();//boite pour le bouton

        //implémentation des combos
        //Grâce aux methodes @Override getString ajoutées aux objets, JComboBox affiche le nom des objets
        ArrayList<Salarie> maListeSalaries = new ArrayList<>();
        maListeSalaries = SalarieDAO.getAll();
        comboSalarie = new JComboBox(maListeSalaries.toArray());

        //action du bouton ajouter : ajouter la conférence
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterConferencier();
            }

        });

        //mise en place des différents éléments
        titrePageBox.add(labelTitrePage);
        confLBox.add(labelConf);
        confTBox.add(textConf);
        salarieLBox.add(labelSalarie);
        salarieTBox.add(comboSalarie);
        confBox.add(confLBox);
        confBox.add(confTBox);
        salarieBox.add(salarieLBox);
        salarieBox.add(salarieTBox);
        checkIBox.add(checkInterne);
        checkEBox.add(checkExterne);
        areaIBox.add(textInterne);
        areaEBox.add(textExterne);
        btnBox.add(btnValider);
        interneBox.add(checkIBox);
        interneBox.add(salarieBox);
        interneBox.add(areaIBox);
        externeBox.add(checkEBox);
        externeBox.add(confBox);
        externeBox.add(areaEBox);
        mainBox.add(titrePageBox);
        mainBox.add(interneBox);
        mainBox.add(externeBox);
        mainBox.add(btnBox);

        this.add(mainBox);

        //mise en forme du Titre de la page
        designLabelTitre(labelTitrePage);

        //mise en forme le bouton
        designButton(btnValider);

        //mise en forme des labels
        designLabel(labelConf);
        designLabel(labelSalarie);
        
        //mise en forme des checkbox
        designCheck(checkInterne);
        designCheck(checkExterne);

        //mise en forme des champs de saisie
        designField(textConf);
        designCombo(comboSalarie);
        designArea(textInterne);
        designArea(textExterne);

    }

    public void ajouterConferencier() {
        JOptionPane jop;
        String msg = "";
        String nomPrenomConferencier;
        Boolean conferencierInterne;
        String blocNoteConferencier;

        //tests de saisie
        if (this.checkInterne.isSelected() && this.checkExterne.isSelected()) {
            msg = "Vous ne pouvez pas cocher Interne et Externe";
        } else if (!this.checkInterne.isSelected() && !this.checkExterne.isSelected()) {
            msg = "Vous devez choisir Interne et Externe";
        } else if (this.checkExterne.isSelected() && this.textConf.getText().isEmpty()) {
            msg = "Vous avez choisi un conférencier Externe, merci de saisir son Nom et son prénom";
        }

        if (msg != "") {
            jop = new JOptionPane();
            jop.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
        } else {
            if (this.checkExterne.isSelected()) {
                nomPrenomConferencier = this.textConf.getText();
                conferencierInterne = this.checkInterne.isSelected();
                blocNoteConferencier = this.textInterne.getText();
            } else {
                nomPrenomConferencier = this.comboSalarie.getSelectedItem().toString();
                conferencierInterne = this.checkExterne.isSelected();
                blocNoteConferencier = this.textExterne.getText();
            }
            Conferencier conferencier = new Conferencier(nomPrenomConferencier, conferencierInterne, blocNoteConferencier);
            ConferencierDAO.insert(conferencier);
            //pour mettre à jour la liste des conférenciers
            ListConferencierPanel.monModel.setData(ConferencierDAO.getAll());
            ListConferencierPanel.monModel.fireTableDataChanged();
        }

    }
}

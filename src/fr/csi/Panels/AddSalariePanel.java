/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.SalarieDAO;
import fr.csi.Database.POJO.Salarie;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class AddSalariePanel extends DesignPanel {

    //les labels
    private JLabel labelTitrePage = new JLabel("Ajouter un salarié");
    private JLabel labelNom = new JLabel("Nom et Prénom");

    //les champs de saisie
    private JTextField textNom = new JTextField();

    //bouton
    private JButton btnValider = new JButton("Valider");

    public AddSalariePanel() {

        Box mainBox = Box.createVerticalBox();//boite principale
        Box titrePageBox = Box.createHorizontalBox();//boite pour le titre de la page
        Box nomLBox = Box.createVerticalBox();//boite pour le label du nom et prénom
        Box nomTBox = Box.createVerticalBox();//boite pour le texte du nom
        Box nomBox = Box.createHorizontalBox();//boite pour les éléments du nom
        Box btnBox = Box.createHorizontalBox();//boite pour le bouton

        //action du bouton ajouter : ajouter la conférence
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterSalarie();
            }
        });

        //mise en place des différents éléments
        titrePageBox.add(labelTitrePage);
        nomLBox.add(labelNom);
        nomTBox.add(textNom);
        nomBox.add(nomLBox);
        nomBox.add(nomTBox);
        btnBox.add(btnValider);
        mainBox.add(titrePageBox);
        mainBox.add(nomBox);
        mainBox.add(btnBox);

        this.add(mainBox);

        //mise en forme du Titre de la page
        designLabelTitre(labelTitrePage);

        //mise en forme le bouton
        designButton(btnValider);

        //mise en forme des labels
        designLabel(labelNom);

        //mise en forme des champs de saisie
        designField(textNom);
    }

    public void ajouterSalarie() {
        JOptionPane jop;
        String msg = "";

        //tests de saisie
        if (this.textNom.getText().isEmpty()) {
            msg = "Vous devez renseigner le nom et le prénom du salarié";
        } 
        if (msg != "") {
            jop = new JOptionPane();
            jop.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
        } else {
            String nomPrenom = this.textNom.getText();
            Salarie salarie = new Salarie(nomPrenom);
            SalarieDAO.insert(salarie);
            //pour mettre à jour la liste des conférences dans la page d'accueil
            ListSalariePanel.monModel.setData(SalarieDAO.getAll());
            ListSalariePanel.monModel.fireTableDataChanged();
        }
    }
}

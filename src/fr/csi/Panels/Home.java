/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.CustomWindow;
import fr.csi.Database.DAO.ConferenceDAO;
import fr.csi.Database.DAO.InscriptionDAO;
import fr.csi.Database.DAO.InscriptionStatutDAO;
import fr.csi.Database.DAO.SalarieDAO;
import static fr.csi.Database.DatabaseUtiles.exec;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.InscriptionStatut;
import fr.csi.Database.POJO.Salarie;
import fr.csi.Models.ConferenceModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ColorModel;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.Border;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN;
import static javax.swing.JTable.AUTO_RESIZE_OFF;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

/**
 *
 * @author Asus
 */
public class Home extends DesignPanel {

    private JLabel labelTitre = new JLabel("Conférences à venir");
    private JLabel labelSearch = new JLabel("Rechercher un Titre");
    private JTextField textSearch = new JTextField();
    private JButton btnSearch = new JButton("Rechercher");
    private JButton btnRAZ = new JButton("Effacer");
    public static ConferenceModel monModel;
    private JLabel labelSalarie = new JLabel("Votre nom");
    private JComboBox comboSalarie;
    private JButton btnSinscrire = new JButton("S'inscrire");
    private JButton btnMesConf = new JButton("Mes Conférences");
    private JButton btnAjouter = new JButton("Ajouter");
    private JButton btnSupprimer = new JButton("Supprimer");

    private Conference conference;//prendra la valeur de la conférence sélectionnée
    private int idConference;//prendra la valeur de l'identifiant de la conférence sélectionnée

    public Home() {
        //Création de boîtes de rangement
        Box mainBox = Box.createVerticalBox();//boite principale
        //mainBox.add(Box.createRigidArea(new Dimension(600,200)));
        Box labelBox = Box.createHorizontalBox();//boite pour le label
        Box searchLBox = Box.createVerticalBox();//boîte pour le label de recherche 
        Box searchTBox = Box.createVerticalBox();//boîte pour le texte de recherche 
        Box searchZBox = Box.createHorizontalBox();//boîte pour la zone de recherche 
        Box searchBBox = Box.createVerticalBox();//boîte pour le bouton de recherche 
        Box razBox = Box.createVerticalBox();//boîte pour le bouton de remise à zéro 
        Box searchBtnBox = Box.createHorizontalBox();//boîte pour les boutons de recherche
        Box searchBox = Box.createVerticalBox();//boîte pour les éléments de recherche
        Box listeBox = Box.createVerticalBox();//boite pour la liste
        Box salarieLBox = Box.createVerticalBox();//boite pour le label du salarié
        Box salarieTBox = Box.createVerticalBox();//boite pour le texte du salarié
        Box salarieBox = Box.createHorizontalBox();//boite pour les éléments du salarié
        Box btnInscrireBox = Box.createVerticalBox();//boite pour le bouton S'inscrire
        Box btnMesConfBox = Box.createVerticalBox();//boite pour le bouton S'inscrire
        Box btnAjouterBox = Box.createVerticalBox();//boite pour le bouton Ajouter
        Box btnSupprimerBox = Box.createVerticalBox();//boite pour le bouton Supprimer
        Box btnBox = Box.createHorizontalBox();//boite pour les boutons

        //mise en forme du label Titre
        designLabelTitre(labelTitre);
        //on ajoute le label dans sa boite        
        labelBox.add(labelTitre);
        
        //mise en forme du label Recharche
        designLabel(labelSearch);
        //on ajoute le label dans sa boite        
        searchLBox.add(labelSearch);

        //mise en forme de la zone de recherche
        designField(textSearch);
        textSearch.setPreferredSize(new Dimension(150, 20));
        //on ajoute la zone à sa boîte
        searchTBox.add(textSearch);
        
        //j'ajoute les éléments de la zone de recherche
        searchZBox.add(searchLBox);
        searchZBox.add(searchTBox);

        //mise en forme des boutons
        designButton(btnSearch);
        designButton(btnRAZ);

        //on ajoute les boutons à leur box
        searchBBox.add(btnSearch);
        razBox.add(btnRAZ);
        searchBtnBox.add(searchBBox);
        searchBtnBox.add(razBox);

        //j'ajoute les éléments de la zone de recherche dans leur boîte
        searchBox.add(searchZBox);
        searchBox.add(searchBtnBox);

        //Action du bouton Rechercher
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane jop = null;
                if (textSearch.getText().isEmpty()) {
                    jop.showMessageDialog(null, "Une erreur s'est produite lors de l'inscription", "Erreur", JOptionPane.ERROR_MESSAGE);
                } else {
                    String recherche = textSearch.getText();
                    monModel.setData(ConferenceDAO.getSearch(recherche));
                    monModel.fireTableDataChanged();
                    repaint();
                }
            }
        });

        //Action du bouton RAZ
        btnRAZ.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                monModel.setData(ConferenceDAO.getAllNew());
                monModel.fireTableDataChanged();
                repaint();
                textSearch.setText("");
            }
        });

        //création d'une ConferenceModel à partir de ma liste de conférences à venir
        ArrayList<Conference> maListeConferences = new ArrayList<>();
        maListeConferences = ConferenceDAO.getAllNew();
        monModel = new ConferenceModel(maListeConferences);

        //Création d'un JTable à partir de mon modèle
        JTable maTable = new JTable(monModel);
        maTable.repaint();
        designTable(maTable);
        //Entête de ma table
        JTableHeader headerTable = maTable.getTableHeader();
        designTableHeader(headerTable);

        //Action de sélection d'une ligne de la table
        maTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //je récupère l'identifiant de la conférence
                int ligne = maTable.getSelectedRow();
                //je récupère la conférence
                conference = monModel.getValue(ligne);
                //je récupère l'identifiant de la conférence
                idConference = conference.getIdConference();
                //je récupère le salarié sélectionné
                Salarie salarie = (Salarie) comboSalarie.getSelectedItem();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        //Création d'un JScrollPane à partir de ma table
        JScrollPane monScroll = new JScrollPane(maTable);
        designScroll(monScroll);

        //j'ajoute la liste dans sa boite
        listeBox.add(monScroll);

        //Création du combo avec la liste des salariés
        ArrayList<Salarie> maListeSalaries = new ArrayList<>();
        maListeSalaries = SalarieDAO.getAll();
        comboSalarie = new JComboBox(maListeSalaries.toArray());
        //Mise en forme de la liste des salariés
        designLabel(labelSalarie);
        designCombo(comboSalarie);
        //j'ajoute la liste dans sa boîte
        salarieLBox.add(labelSalarie);
        salarieTBox.add(comboSalarie);
        salarieBox.add(salarieLBox);
        salarieBox.add(salarieTBox);

        //mise en forme du bouton S'inscrire
        designButton(btnSinscrire);
        //on ajoute le bouton dans sa boite        
        btnInscrireBox.add(btnSinscrire);

        //Action du bouton S'inscrire
        btnSinscrire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Salarie salarie = (Salarie) comboSalarie.getSelectedItem();
                inscrire(salarie, conference);
            }
        });
        
        //mise en forme du bouton Mes Conférences
        designButton(btnMesConf);
        //on ajoute le bouton dans sa boite        
        btnMesConfBox.add(btnMesConf);

        //Action du bouton S'inscrire
        btnMesConf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Salarie salarie = (Salarie) comboSalarie.getSelectedItem();
                int idSalarie=salarie.getIdSalarie();
                mesConf(idSalarie);
            }
        });

        //mise en forme du bouton Ajouter
        designButton(btnAjouter);
        //on ajoute le bouton dans sa boite        
        btnAjouterBox.add(btnAjouter);

        //Action du bouton S'inscrire
        btnAjouter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouter();
            }
        });

        //mise en forme du bouton Ajouter
        designButton(btnSupprimer);
        //on ajoute le bouton dans sa boite        
        btnSupprimerBox.add(btnSupprimer);

        //Action du bouton S'inscrire
        btnSupprimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                supprimer(idConference, conference);
            }
        });
        btnBox.add(btnSinscrire);
        btnBox.add(btnMesConfBox);
        btnBox.add(btnAjouter);
        btnBox.add(btnSupprimer);

        //je mets les boites dans la boite principale et je l'affiche
        mainBox.add(labelBox);
        mainBox.add(searchBox);
        mainBox.add(listeBox);
        mainBox.add(salarieBox);
        mainBox.add(btnBox);
        
        this.add(mainBox);
        
    }

    private void inscrire(Salarie _s, Conference _c) {
        JOptionPane jop = null;
        //Nombre de places dans la salle
        int nbPlaces = _c.getSalle().getNbPlaceSalle();

        //Nombre d'inscrits à la conférence
        ArrayList<Salarie> listeSalarieInscrits = new ArrayList<>();
        listeSalarieInscrits = InscriptionDAO.getSalarieInscrits(_c.getIdConference());
        int nbInscrits = listeSalarieInscrits.size();

        //Inscription à la conférence et récupération de l'id pour créer un statut
        InscriptionDAO.insert(_s, _c);

        //Mise à jour du statut de l'inscription "inscrit" ou "attente"
        if (nbInscrits < nbPlaces) {
            InscriptionStatutDAO.insert(2);
            jop.showMessageDialog(null, "Vous êtes inscrit à la conférence", "Information", JOptionPane.INFORMATION_MESSAGE);
        } else {
            InscriptionStatutDAO.insert(3);
            jop.showMessageDialog(null, "Votre êtes inscrit en liste d'attente", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void mesConf(int i) {
        CustomWindow cw = (CustomWindow) this.getTopLevelAncestor();
        cw.changerPanel("mesConferencesPanel",i);
    }
    
    private void ajouter() {
        CustomWindow cw = (CustomWindow) this.getTopLevelAncestor();
        cw.changerPanel("ajouterConferencePanel",0);
    }

    private void supprimer(int _id, Conference _c) {
        //Si la conférence n'a pas eu lieu je récupère la liste des salariés inscrits
        //pour leur envoyer un message
        JOptionPane jop = null;
        Calendar dateConf = _c.getDateConference();
        Calendar aujourdhui = Calendar.getInstance();
        if (dateConf.compareTo(aujourdhui) < 0) {
            ArrayList<Salarie> listeSalarieInscrits = new ArrayList<>();
            listeSalarieInscrits = InscriptionDAO.getSalarieInscrits(_id);
        }

        //je supprime les inscription à cette conférence
        InscriptionDAO.deleteInscriptionConf(_id);

        //je supprime la conférence
        ConferenceDAO.delete(_id);

        jop.showMessageDialog(null, "Votre conférence a été supprimée", "Information", JOptionPane.INFORMATION_MESSAGE);

        //Je remets à jour les pages contenant les listes de conférences
        this.monModel.setData(ConferenceDAO.getAllNew());
        this.monModel.fireTableDataChanged();
        this.repaint();
        this.revalidate();
        ListConferencePanel.monModel.setData(ConferenceDAO.getAll());
        ListConferencePanel.monModel.fireTableDataChanged();
        ListOldConferencePanel.monModel.setData(ConferenceDAO.getAllOld());
        ListOldConferencePanel.monModel.fireTableDataChanged();

    }
}

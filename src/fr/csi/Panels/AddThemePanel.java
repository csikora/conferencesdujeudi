/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.ThemeDAO;
import fr.csi.Database.POJO.Theme;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class AddThemePanel extends DesignPanel{
    //les labels
    private JLabel labelTitrePage = new JLabel("Ajouter un thème");
    private JLabel labelNom = new JLabel("Désignation");

    //les champs de saisie
    private JTextField textNom = new JTextField();

    //bouton
    private JButton btnValider = new JButton("Valider");
    
    public AddThemePanel(){
        Box mainBox = Box.createVerticalBox();//boite principale
        Box titrePageBox = Box.createHorizontalBox();//boite pour le titre de la page
        Box nomLBox = Box.createVerticalBox();//boite pour le label de la désignation
        Box nomTBox = Box.createVerticalBox();//boite pour le texte de la désignation
        Box nomBox = Box.createHorizontalBox();//boite pour les éléments de la désignation
        Box btnBox = Box.createHorizontalBox();//boite pour le bouton

        //action du bouton ajouter : ajouter la conférence
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterTheme();
            }
        });
        
        //mise en place des différents éléments
        titrePageBox.add(labelTitrePage);
        nomLBox.add(labelNom);
        nomTBox.add(textNom);
        nomBox.add(nomLBox);
        nomBox.add(nomTBox);
        btnBox.add(btnValider);
        mainBox.add(titrePageBox);
        mainBox.add(nomBox);
        mainBox.add(btnBox);

        this.add(mainBox);

        //mise en forme du Titre de la page
        designLabelTitre(labelTitrePage);

        //mise en forme le bouton
        designButton(btnValider);

        //mise en forme des labels
        designLabel(labelNom);

        //mise en forme des champs de saisie
        designField(textNom);
    }
    
    public void ajouterTheme() {
        JOptionPane jop;
        String msg = "";

        //tests de saisie
        if (this.textNom.getText().isEmpty()) {
            msg = "Vous devez renseigner la désignation du thème";
        } 
        if (msg != "") {
            jop = new JOptionPane();
            jop.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
        } else {
            String designation = this.textNom.getText();
            Theme theme = new Theme(designation);
            ThemeDAO.insert(theme);
            //pour mettre à jour la liste des conférences dans la page d'accueil
            ListThemePanel.monModel.setData(ThemeDAO.getAll());
            ListThemePanel.monModel.fireTableDataChanged();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.almeri.Panels;

import fr.almeri.Database.DAO.ConferenceDAO;
import fr.almeri.Database.DAO.ConferencierDAO;
import fr.almeri.Database.DAO.ThemeDAO;
import fr.almeri.Database.POJO.Conference;
import fr.almeri.Database.POJO.Conferencier;
import fr.almeri.Database.POJO.Theme;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author Marie
 */
public class PanelConferenceAdd extends JPanel {
    
    JFrame window;
    JPanel pan;
    JLabel label, label1, label2, label3, label4;
    JTextField champ1;
    JFormattedTextField champ2;
    JComboBox<Theme> champ3;
    JComboBox<Conferencier> champ4;
    JButton but;
    
    public PanelConferenceAdd() {
        window = (JFrame) SwingUtilities.getWindowAncestor(this);
        pan = this;
        //On créé des box pour organiser nos labels et champs
        Box vertical = Box.createVerticalBox();
        this.add(vertical);
        label = new JLabel("Nouvelle conférence");
        vertical.add(label);
        //Pour chq champ, on créé une ligne avec un label et un champ texte ou combo
        Box ligne1 = Box.createHorizontalBox();
        label1 = new JLabel("Titre de la conférence ");
        ligne1.add(label1);
        champ1 = new JTextField();
        ligne1.add(champ1);
        vertical.add(ligne1);
        
        Box ligne2 = Box.createHorizontalBox();
        label2 = new JLabel("Date de la conférence (dd/MM/yyyy) ");
        ligne2.add(label2);
        //On oblige l'utilisateur à écrire des données sous un certain format dans notre champ de saisie 
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        champ2 = new JFormattedTextField(format);
        champ2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                //Action à faire lorsque le focus est sur le champ date
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                //Action à faire lorsqu'on perd le focus
                //On valide le format sinon on change la valeur par null
                if (!champ2.isEditValid()) {
                    champ2.setValue(null);
                }
            }
        });
        ligne2.add(champ2);
        vertical.add(ligne2);
        
        Box ligne3 = Box.createHorizontalBox();
        label3 = new JLabel("Thème de la conférence ");
        ligne3.add(label3);
        //On récupère la liste des thèmes dans notre base de données
        ArrayList<Theme> ts = ThemeDAO.getAll();
        //On l'ajoute à notre comboxBox
        //Attention, il faut avoir redéfinit la méthode toString() dans la classe Theme
        //pour afficher la designationTheme et non la référence de l'objet
        //Voici comment faire : 
        //@Override
        //public String toString() {
        //    return designationTheme;
        //}
        //JComboBox prend en param un tableau. 
        //On transforme la liste en tableau à l'aide de la méthode to Array()
        champ3 = new JComboBox(ts.toArray());
        ligne3.add(champ3);
        vertical.add(ligne3);
        
        Box ligne4 = Box.createHorizontalBox();
        label4 = new JLabel("Conférencier ");
        ligne4.add(label4);
        ArrayList<Conferencier> conferenciers = ConferencierDAO.getAll();
        champ4 = new JComboBox(conferenciers.toArray());
        ligne4.add(champ4);
        vertical.add(ligne4);
        
        Box ligne5 = Box.createHorizontalBox();
        but = new JButton("AJOUTER");
        but.setBounds(0, 0, 100, 50);
        ligne5.add(but);
        //Au clic sur le bouton AJOUTER
        but.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //On récupère notre date sous format String
                String dateString = champ2.getText();
                //On le transforme en Calendar pour pouvoir créer un objet Conférence
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(sdf.parse(dateString));
                    //On créé l'objet à partir de nos champs
                    Conference c = new Conference(champ1.getText(), cal, ((Theme) champ3.getSelectedItem()), ((Conferencier) champ4.getSelectedItem()));
                    //On insère l'objet
                    ConferenceDAO.insert(c);
                    //On affiche un message comme quoi tout s'est bien passé
                    JOptionPane.showMessageDialog(window, "La conférence vient d'être ajoutée.", "Ajout", JOptionPane.INFORMATION_MESSAGE);
                    System.out.println("Ajout de " + champ1.getText() + " sur " + champ3.getSelectedItem().toString() + " le " + champ2.getText() + " par " + champ4.getSelectedItem().toString() + ", " + ((Conferencier) champ4.getSelectedItem()).getIdConferencier());
                    //On récupère notre fenêtre parente  
                    JFrame jf = (JFrame) pan.getTopLevelAncestor();
                    //On change le panel afin d'afficher à nouveau la liste des conf
                    jf.setContentPane(new PanelConferencesList());
                    jf.revalidate();
                    jf.repaint();
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(window, "Impossible d'ajouter la conférence.", "Erreur", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(PanelConferenceAdd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        });
        vertical.add(ligne5);
    }
}

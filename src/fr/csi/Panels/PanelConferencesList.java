﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.almeri.Panels;

import fr.almeri.Database.DAO.ConferenceDAO;
import fr.almeri.Database.DatabaseUtilities;
import fr.almeri.Database.POJO.Conference;
import fr.almeri.Database.POJO.Conferencier;
import fr.almeri.Database.POJO.Theme;
import fr.almeri.Models.ConferenceModel;
import fr.almeri.Models.ConferenceModelLite;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author Marie
 */
public class PanelConferencesList extends JPanel {

    Window window;
    JTextField search;
    ConferenceModel tableModel;
    JTable t;
    Box vertical;
    JButton but1;

    public PanelConferencesList() {
        window = SwingUtilities.getWindowAncestor(this);

        vertical = Box.createVerticalBox();
        this.add(vertical);

        Box ligne1 = Box.createHorizontalBox();
        JLabel l = new JLabel("Recherche par titre : ");
        search = new JTextField();
        ligne1.add(l);
        ligne1.add(search);
        vertical.add(ligne1);
        search.setSize(200, 100);
        search.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                searchConf(false);
            }
        });
        searchConf(true);

        Box actionsBar = Box.createHorizontalBox();
        but1 = new JButton("SUPPRIMER");
        but1.setBounds(0, 0, 100, 50);
        //Rend le bouton inactif tant qu'on n'a pas cliqué sur une ligne
        but1.setEnabled(false);
        actionsBar.add(but1);
        but1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Ouverture d'une boite de dialogue pour demander confirmation avant suppression
                int opt = JOptionPane.showConfirmDialog(window, "Etes-vous certain de vouloir supprimer la conférence ?", "Suppression", JOptionPane.OK_CANCEL_OPTION);

                //Si l'utilisateur a cliqué sur ok sinon rien
                if (opt == JOptionPane.OK_OPTION) {
                    //On récupère l'indice de la ligne sélectionnée
                    int idRow = t.getSelectedRow();
                    //On récupère la conférence via une méthode que nous avons créée dans la classe
                    //ConferenceModel
                    //Celle-ci renvoie l'objet Conference qui se situe à la position sélectionnée dans la liste
                    Conference c = ((ConferenceModel) t.getModel()).getValue(idRow);
                    System.out.println("row selected = " + idRow);
                    System.out.println("conf selected = " + c.getIdConference());
                    System.out.println("conf selected = " + c.getTitreConference());
                    //On supprime la conférence sélectionnée
                    ConferenceDAO.delete(c.getIdConference());

                    //RAFRAICHIR LES DONNEES DANS LE TABLEAU                    
                    //On récupère la liste des conférences à nouveau
                    ArrayList<Conference> conferences = ConferenceDAO.getAll();
                    //On utilise une méthode que l'on a créée dans ConferenceModel 
                    //pour modifier la liste initialement passée en paramètre dans le constructeur
                    //A présent, elle sera égale à la nouvelle liste qui ne contient pas la conférence
                    //que nous venons de supprimer
                    tableModel.setDatas(conferences);
                    //On appelle la méthode qui permet d'indiquer que les données ont changé
                    tableModel.fireTableDataChanged();
                    //On rafraichit notre tableau
                    t.revalidate();
                    t.repaint();
                }

            }

        });
        vertical.add(actionsBar);
    }

    //
    private void searchConf(boolean init) {
        ArrayList<Conference> conf = ConferenceDAO.searchConf(search.getText());
        displayList(conf, init);
    }

    private void displayList(ArrayList<Conference> conferences, boolean init) {
        if (init) {
            tableModel = new ConferenceModel(conferences);
            t = new JTable(tableModel);
            t.setBounds(0, 0, 1000, 3000);
            t.setAutoCreateRowSorter(true);
            t.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

            //Listener pour écouter les clics sur le tableau
            t.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    //Rendre le bouton SUPPRIMER actif lorsqu'on clique
                    //quelque par sur le tableau
                    but1.setEnabled(true);
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    //Do nothing 
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    //Do nothing 
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    //Do nothing 
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    //Do nothing
                }
            });
            //Conteneur du tableau - gère l'ascenceur
            JScrollPane jsp = new JScrollPane(t);
            jsp.setBounds(0, 0, 1000, 3000);

            Box ligne2 = Box.createHorizontalBox();
            ligne2.add(jsp);
            vertical.add(ligne2);
        }
        tableModel.setDatas(conferences);
        tableModel.fireTableDataChanged();
        t.revalidate();
        t.repaint();
    }

}
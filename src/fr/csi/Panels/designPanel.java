/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.JTable.AUTO_RESIZE_OFF;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Asus
 */
public class designPanel extends JPanel {

    private Font fontLabelTitre = new Font("Calibri", Font.BOLD, 24);

    public designPanel() {
//couleur du panel
        this.setBackground(new Color(58, 151, 139));
    }

    public void designTable(JTable table) {
        table.setFont(new Font("Calibri",Font.PLAIN,12));
        //la table sera traible et redimensionnable
        table.setAutoCreateRowSorter(true);
        //table.setAutoResizeMode(AUTO_RESIZE_OFF);
    }

    public void designTableHeader(JTableHeader tableHeader) {
        tableHeader.setBackground(new Color(206,181,144));
        tableHeader.setFont(new Font("Calibri",Font.PLAIN,14));
    }

    public void designButton(JButton button) {
        button.setBounds(400, 50, 30, 10);
        button.setForeground(new Color(198,46,117));
        button.setBackground(new Color(206,181,144));
        button.setBorderPainted(false);
        button.setPreferredSize(new Dimension(400,100));
    }

    public void designScroll(JScrollPane scroll) {
        scroll.getViewport().setBackground(new Color(206,181,144));
    }

    public void designLabel(JLabel label) {
        label.setFont(new Font("Calibri",Font.BOLD,14));
        label.setForeground(new Color(206,181,144));
        label.setPreferredSize(new Dimension(150,20));
    }

    public void designLabelTitre(JLabel labelTitre) {
        labelTitre.setFont(fontLabelTitre);
        labelTitre.setForeground(new Color(206,181,144));
        labelTitre.setPreferredSize(new Dimension(600,60));
    }
    
    public void designField(JTextField textField){
        textField.setFont(new Font("Calibri",Font.BOLD,14));
    }
    
    public void designDateField(JFormattedTextField textField){
        textField.setFont(new Font("Calibri",Font.BOLD,14));
    }
    
    public void designCombo(JComboBox combo){
        combo.setFont(new Font("Calibri",Font.BOLD,14));
    }
    
    public void designArea(JTextArea area){
        area.setFont(new Font("Calibri",Font.BOLD,14));
    }
    
    public void designCheck(JCheckBox check){
        check.setFont(new Font("Calibri",Font.BOLD,14));
        check.setForeground(new Color(206,181,144));
        check.setBackground(new Color(58, 151, 139));
    }

}

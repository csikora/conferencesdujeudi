/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.ConferencierDAO;
import fr.csi.Database.POJO.Conferencier;
import fr.csi.Models.ConferencierModel;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Asus
 */
public class ListConferencierPanel extends DesignPanel{
    private JLabel labelTitre = new JLabel("Liste des conférenciers");
    public static ConferencierModel monModel;
    
    public ListConferencierPanel(){
        //Création de boîtes de rangement
        Box mainBox=Box.createVerticalBox();//boite principale
        Box labelBox=Box.createHorizontalBox();//boite pour le label
        Box listeBox=Box.createVerticalBox();//boite pour la liste
        
        //mise en forme du label Titre
        designLabelTitre(labelTitre);
        //on ajoute le label dans sa boite        
        labelBox.add(labelTitre);
        
        //création d'une ConferenceModel à partir de ma liste de conférences à venir
        ArrayList<Conferencier> maListeConferenciers = new ArrayList<>();
        maListeConferenciers=ConferencierDAO.getAll();
        monModel = new ConferencierModel(maListeConferenciers);
        
        //Création d'un JTable à partir de mon modèle
        JTable maTable = new JTable(monModel);
        maTable.repaint();
        designTable(maTable);
        //Entête de ma table
        JTableHeader headerTable=maTable.getTableHeader();
        designTableHeader(headerTable);
        
        //Création d'un JScrollPane à partir de ma table
        JScrollPane monScroll = new JScrollPane(maTable);
        designScroll(monScroll);
        
        //j'ajoute la liste dans sa boite
        listeBox.add(monScroll);
        
        //je mets les boites dans la boite principale et je l'affiche
        mainBox.add(labelBox);
        mainBox.add(listeBox);
        this.add(mainBox);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.SalarieDAO;
import fr.csi.Database.DAO.SalleDAO;
import fr.csi.Database.POJO.Salarie;
import fr.csi.Database.POJO.Salle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Asus
 */
public class AddSallePanel extends DesignPanel{
    //les labels
    private JLabel labelTitrePage = new JLabel("Ajouter un salarié");
    private JLabel labelNom = new JLabel("Nom et Prénom");
    private JLabel labelPlaces = new JLabel("Nombre de places");

    //les champs de saisie
    private JTextField textNom = new JTextField();
    private JTextField textPlaces = new JTextField();

    //bouton
    private JButton btnValider = new JButton("Valider");
    
    public AddSallePanel(){
        Box mainBox = Box.createVerticalBox();//boite principale
        Box titrePageBox = Box.createHorizontalBox();//boite pour le titre de la page
        Box nomLBox = Box.createVerticalBox();//boite pour le label du nom et prénom
        Box nomTBox = Box.createVerticalBox();//boite pour le texte du titre
        Box nomBox = Box.createHorizontalBox();//boite pour les éléments du titre
        Box placesLBox = Box.createVerticalBox();//boite pour le label du nombre de places
        Box placesTBox = Box.createVerticalBox();//boite pour le texte du nombre de places
        Box placesBox = Box.createHorizontalBox();//boite pour les éléments du nombre de places
        Box btnBox = Box.createHorizontalBox();//boite pour le bouton

        //action du bouton ajouter : ajouter la conférence
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ajouterSalle();
            }
        });
        
        //mise en place des différents éléments
        titrePageBox.add(labelTitrePage);
        nomLBox.add(labelNom);
        nomTBox.add(textNom);
        nomBox.add(nomLBox);
        nomBox.add(nomTBox);
        placesLBox.add(labelPlaces);
        placesTBox.add(textPlaces);
        placesBox.add(placesLBox);
        placesBox.add(placesTBox);
        btnBox.add(btnValider);
        mainBox.add(titrePageBox);
        mainBox.add(nomBox);
        mainBox.add(placesBox);
        mainBox.add(btnBox);

        this.add(mainBox);

        //mise en forme du Titre de la page
        designLabelTitre(labelTitrePage);

        //mise en forme le bouton
        designButton(btnValider);

        //mise en forme des labels
        designLabel(labelNom);
        designLabel(labelPlaces);

        //mise en forme des champs de saisie
        designField(textNom);
        designField(textPlaces);
    }
    
    public void ajouterSalle() {
        JOptionPane jop;
        String msg = "";

        //tests de saisie
        if (this.textNom.getText().isEmpty()) {
            msg = "Vous devez renseigner le nom de la salle";
        } else if(this.textNom.getText().isEmpty()){
            msg = "Vous devez renseigner le nombre de places";
        }
        if (msg != "") {
            jop = new JOptionPane();
            jop.showMessageDialog(null, msg, "Erreur", JOptionPane.ERROR_MESSAGE);
        } else {
            String nomSalle = this.textNom.getText();
            int nbPlaceSalle = Integer.parseInt(this.textPlaces.getText());
            Salle salle = new Salle(nomSalle,nbPlaceSalle);
            SalleDAO.insert(salle);
            //pour mettre à jour la liste des conférences dans la page d'accueil
            ListSallePanel.monModel.setData(SalleDAO.getAll());
            ListSallePanel.monModel.fireTableDataChanged();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Panels;

import fr.csi.Database.DAO.ConferenceDAO;
import fr.csi.Database.DAO.InscriptionDAO;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.Inscription;
import fr.csi.Models.ConferenceModel;
import static fr.csi.Panels.ListConferencePanel.monModel;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Asus
 */
public class MyConferencePanel extends DesignPanel{
    private JLabel labelTitre = new JLabel("Mes conférences");
    public static ConferenceModel monModel;
    
    public MyConferencePanel(int i){
        //Création de boîtes de rangement
        Box mainBox=Box.createVerticalBox();//boite principale
        Box labelBox=Box.createHorizontalBox();//boite pour le label
        Box listeBox=Box.createVerticalBox();//boite pour la liste
        
        //mise en forme du label Titre
        designLabelTitre(labelTitre);
        //on ajoute le label dans sa boite        
        labelBox.add(labelTitre);
        
        //création d'une ConferenceModel à partir de ma liste de conférences à venir
        ArrayList<Conference> mesConferences = new ArrayList<>();
        ArrayList<Inscription> mesInscriptions = new ArrayList<>();
        mesInscriptions=InscriptionDAO.getMine(i);
        for (Inscription inscription : mesInscriptions){
            Conference maConference=inscription.getConference();
            mesConferences.add(maConference);
        }
        monModel = new ConferenceModel(mesConferences);
        
        //Création d'un JTable à partir de mon modèle
        JTable maTable = new JTable(monModel);
        maTable.repaint();
        designTable(maTable);
        //Entête de ma table
        JTableHeader headerTable=maTable.getTableHeader();
        designTableHeader(headerTable);
        
        //Création d'un JScrollPane à partir de ma table
        JScrollPane monScroll = new JScrollPane(maTable);
        designScroll(monScroll);
        
        //j'ajoute la liste dans sa boite
        listeBox.add(monScroll);
        
        //je mets les boites dans la boite principale et je l'affiche
        mainBox.add(labelBox);
        mainBox.add(listeBox);
        this.add(mainBox);
    }
}

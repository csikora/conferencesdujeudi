/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi;

import fr.csi.Panels.AddConferencePanel;
import fr.csi.Panels.AddConferencierPanel;
import fr.csi.Panels.AddSalariePanel;
import fr.csi.Panels.AddSallePanel;
import fr.csi.Panels.AddThemePanel;
import fr.csi.Panels.Home;
import fr.csi.Panels.ListConferencePanel;
import fr.csi.Panels.ListConferencierPanel;
import fr.csi.Panels.ListOldConferencePanel;
import fr.csi.Panels.ListSalariePanel;
import fr.csi.Panels.ListSallePanel;
import fr.csi.Panels.ListThemePanel;
import fr.csi.Panels.MyConferencePanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import static java.awt.PageAttributes.ColorType.COLOR;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 *
 * @author Asus
 */
public class CustomWindow extends JFrame implements ActionListener {

    /*
    Attributs
     */
    private JMenuBar menuBar = new JMenuBar();
    private JMenu fichierMenu = new JMenu("Fichier");
    private JMenu conferenceMenu = new JMenu("Conférences");
    private JMenu salarieMenu = new JMenu("Salariés");
    private JMenu salleMenu = new JMenu("Salles");
    private JMenu themeMenu = new JMenu("Thèmes");
    private JMenu conferencierMenu = new JMenu("Conférenciers");
    private JMenuItem accueilItem = new JMenuItem("Accueil");
    private JMenuItem fermerItem = new JMenuItem("Fermer");
    private JMenuItem listeConferencesItem = new JMenuItem("Liste des conférences");
    private JMenuItem anciennesConferencesItem = new JMenuItem("Anciennes conférences");
    private JMenuItem ajouterConferenceItem = new JMenuItem("Ajouter une conférence");
    private JMenuItem listeSalariesItem = new JMenuItem("Liste des salariés");
    private JMenuItem ajouterSalarieItem = new JMenuItem("Ajouter un salarié");
    private JMenuItem listeSallesItem = new JMenuItem("Liste des salles");
    private JMenuItem ajouterSalleItem = new JMenuItem("Ajouter une salle");
    private JMenuItem listeConferenciersItem = new JMenuItem("Liste des conférenciers");
    private JMenuItem ajouterConferencierItem = new JMenuItem("Ajouter un conférencier");
    private JMenuItem listeThemesItem = new JMenuItem("Liste des thèmes");
    private JMenuItem ajouterThemeItem = new JMenuItem("Ajouter un thème");

    private Home accueilPanel = new Home();
    private AddConferencePanel ajouterConferencePanel = new AddConferencePanel();
    private AddConferencierPanel ajouterConferencierPanel = new AddConferencierPanel();
    private AddSalariePanel ajouterSalariePanel = new AddSalariePanel();
    private AddSallePanel ajouterSallePanel = new AddSallePanel();
    private AddThemePanel ajouterThemePanel = new AddThemePanel();
    private ListConferencePanel listeConferencePanel = new ListConferencePanel();
    private ListConferencierPanel listeConferencierPanel = new ListConferencierPanel();
    private ListOldConferencePanel listeOldConferencePanel = new ListOldConferencePanel();
    private ListSalariePanel listeSalariePanel = new ListSalariePanel();
    private ListSallePanel listeSallePanel = new ListSallePanel();
    private ListThemePanel listeThemePanel = new ListThemePanel();
    private MyConferencePanel mesConferencesPanel;
    private int i;

    /*Constructeur
    atribuer un nom, une dimension, elle sera visible
     */
    public CustomWindow(String _title, int _width, int _height) {
        //changement de l'icone
        URL adIcone = CustomWindow.class.getResource("images/icone.jpg");
        Image icone = Toolkit.getDefaultToolkit().getImage(adIcone);
        this.setIconImage(icone);
        this.setTitle(_title);
        this.setSize(_width, _height);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Je mets ma barre de menu et je la rend visible
        menuBar.setBackground(new Color(206, 181, 144));
        menuBar.setVisible(true);

        //mise en forme des menus
        designMenu(fichierMenu);
        designMenu(conferenceMenu);
        designMenu(salarieMenu);
        designMenu(salleMenu);
        designMenu(conferencierMenu);
        designMenu(themeMenu);

        //mise en forme des items
        designItem(accueilItem);
        designItem(fermerItem);
        designItem(listeConferencesItem);
        designItem(anciennesConferencesItem);
        designItem(ajouterConferenceItem);
        designItem(listeSalariesItem);
        designItem(ajouterSalarieItem);
        designItem(listeSallesItem);
        designItem(ajouterSalleItem);
        designItem(listeConferenciersItem);
        designItem(ajouterConferencierItem);
        designItem(listeThemesItem);
        designItem(ajouterThemeItem);

        //J'affecte le Listener et les actions aux items
        accueilItem.addActionListener(this);
        fermerItem.addActionListener(this);
        fermerItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        listeConferencesItem.addActionListener(this);
        anciennesConferencesItem.addActionListener(this);
        ajouterConferenceItem.addActionListener(this);
        listeSalariesItem.addActionListener(this);
        ajouterSalarieItem.addActionListener(this);
        listeSallesItem.addActionListener(this);
        ajouterSalleItem.addActionListener(this);
        listeConferenciersItem.addActionListener(this);
        ajouterConferencierItem.addActionListener(this);
        listeThemesItem.addActionListener(this);
        ajouterThemeItem.addActionListener(this);

        //J'affecte mes items à leur menu
        fichierMenu.add(accueilItem);
        fichierMenu.add(fermerItem);
        conferenceMenu.add(listeConferencesItem);
        conferenceMenu.add(anciennesConferencesItem);
        conferenceMenu.add(ajouterConferenceItem);
        salarieMenu.add(listeSalariesItem);
        salarieMenu.add(ajouterSalarieItem);
        salleMenu.add(listeSallesItem);
        salleMenu.add(ajouterSalleItem);
        conferencierMenu.add(listeConferenciersItem);
        conferencierMenu.add(ajouterConferencierItem);
        themeMenu.add(listeThemesItem);
        themeMenu.add(ajouterThemeItem);

        //Jaffecte mes menus à ma barre de menu
        menuBar.add(fichierMenu);
        menuBar.add(conferenceMenu);
        menuBar.add(salarieMenu);
        menuBar.add(salleMenu);
        menuBar.add(conferencierMenu);
        menuBar.add(themeMenu);

        //J'affecte ma barre de menu à ma fenêtre
        this.setJMenuBar(menuBar);

        //initialisation de la fenêtre avec le panel HOME, ensuite on rafraîchit 
        this.setContentPane(accueilPanel);
        this.repaint();
        this.revalidate();
        

        //on rend toute la fenêtre visible
        this.setVisible(true);
    }

    //les méthodes
    //Actions des sous-menus
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == fermerItem) {
            System.exit(0);
        } else if (e.getSource() == accueilItem) {
            this.changerPanel("accueilPanel",0);
        } else if (e.getSource() == ajouterConferenceItem) {
            this.changerPanel("ajouterConferencePanel",0);
        } else if (e.getSource() == ajouterConferencierItem) {
            this.changerPanel("ajouterConferencierPanel",0);
        } else if (e.getSource() == ajouterSalarieItem) {
            this.changerPanel("ajouterSalariePanel",0);
        } else if (e.getSource() == ajouterSalleItem) {
            this.changerPanel("ajouterSallePanel",0);
        } else if (e.getSource() == ajouterThemeItem) {
            this.changerPanel("ajouterThemePanel",0);
        } else if (e.getSource() == listeConferencesItem) {
            this.changerPanel("listeConferencePanel",0);
        } else if (e.getSource() == listeConferenciersItem) {
            this.changerPanel("listeConferencierPanel",0);
        } else if (e.getSource() == anciennesConferencesItem) {
            this.changerPanel("listeOldConferencePanel",0);
        } else if (e.getSource() == listeSalariesItem) {
            this.changerPanel("listeSalariePanel",0);
        } else if (e.getSource() == listeSallesItem) {
            this.changerPanel("listeSallePanel",0);
        } else if (e.getSource() == listeThemesItem) {
            this.changerPanel("listeThemePanel",0);
        } else {
            this.changerPanel("accueilPanel",0);
        }
    }

    //Changer le panel
    public void changerPanel(String choix,int i) {
        switch (choix) {
            case "accueilPanel":
                this.setContentPane(accueilPanel);
                break;
            case "ajouterConferencePanel":
                this.setContentPane(ajouterConferencePanel);
                break;
            case "ajouterConferencierPanel":
                this.setContentPane(ajouterConferencierPanel);
                break;
            case "ajouterSalariePanel":
                this.setContentPane(ajouterSalariePanel);
                break;
            case "ajouterSallePanel":
                this.setContentPane(ajouterSallePanel);
                break;
            case "ajouterThemePanel":
                this.setContentPane(ajouterThemePanel);
                break;
            case "listeConferencePanel":
                this.setContentPane(listeConferencePanel);
                break;
            case "listeConferencierPanel":
                this.setContentPane(listeConferencierPanel);
                break;
            case "listeOldConferencePanel":
                this.setContentPane(listeOldConferencePanel);
                break;
            case "listeSalariePanel":
                this.setContentPane(listeSalariePanel);
                break;
            case "listeSallePanel":
                this.setContentPane(listeSallePanel);
                break;
            case "listeThemePanel":
                this.setContentPane(listeThemePanel);
                break;
            case "mesConferencesPanel":
                mesConferencesPanel=new MyConferencePanel(this.i);
                this.setContentPane(mesConferencesPanel);
                break;

            default:
                break;
        }
        this.repaint();
        this.revalidate();
    }

    public void designMenu(JMenu menu) {
        Font fontMenu = new Font("Calibri", Font.BOLD, 14);
        menu.setFont(fontMenu);
        menu.setForeground(new Color(198, 46, 117));
    }

    public void designItem(JMenuItem item) {
        Font fontItem = new Font("Calibri", Font.ITALIC, 12);
        item.setFont(fontItem);
        item.setForeground(new Color(198, 46, 117));
        item.setBackground(new Color(206, 181, 144));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import fr.csi.Database.DatabaseUtiles;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.Inscription;
import fr.csi.Database.POJO.Salarie;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class InscriptionDAO {
    //récupération de la liste des inscriptions de la BDD
    public static ArrayList<Inscription> getAll(){
        ArrayList<Inscription> maListeInscriptions = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Inscription";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                int idInscription=resultat.getInt("idInscription");
                Salarie salarie=SalarieDAO.get(resultat.getInt("idSalarie"));
                Conference conference=ConferenceDAO.get(resultat.getInt("idConference"));
                Inscription inscription = new Inscription(idInscription,salarie,conference);
                maListeInscriptions.add(inscription);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeInscriptions;
    }
    
    //récupération d'une inscription de la BDD
    public static Inscription get(int _idInscription){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Inscription inscription = null;
        String query="SELECT * FROM Inscription WHERE idInscription=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idInscription);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idInscription=resultat.getInt("idInscription");
                Salarie salarie=SalarieDAO.get(resultat.getInt("idSalarie"));
                Conference conference=ConferenceDAO.get(resultat.getInt("idConference"));
                inscription = new Inscription(idInscription,salarie,conference);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inscription;
    }
    
    //récupération conférences d'un salarié
    public static ArrayList<Inscription> getMine(int _idSalarie){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Inscription inscription = null;
        ArrayList<Inscription> listeInscriptions = new ArrayList<>();
        String query="SELECT * FROM Inscription WHERE idSalarie=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idSalarie);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idInscription=resultat.getInt("idInscription");
                Salarie salarie=SalarieDAO.get(resultat.getInt("idSalarie"));
                Conference conference=ConferenceDAO.get(resultat.getInt("idConference"));
                inscription = new Inscription(idInscription,salarie,conference);
                listeInscriptions.add(inscription);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listeInscriptions;
    }
    
    //récupération le la liste des salariés inscrits à une cobférence
    public static ArrayList<Salarie> getSalarieInscrits(int _idConference){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        ArrayList<Salarie> listeSalarieInscrits = new ArrayList<>();
        String query="SELECT * FROM Inscription WHERE idConference=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idConference);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                Salarie salarie=SalarieDAO.get(resultat.getInt("idSalarie"));
                listeSalarieInscrits.add(salarie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listeSalarieInscrits;
    }
    
    //ajout d'une inscription en BDD
    public static void insert(Salarie _salarie,Conference _conference){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Inscription"
                + "(idSalarie,idConference)"
                + "VALUES (?,?)";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _salarie.getIdSalarie());
            ps.setInt(2, _conference.getIdConference());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre inscription est enregistrée","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'inscription","Erreur",JOptionPane.ERROR_MESSAGE);
        
        }
    }
    
    //suppression d'une inscription en BDD
    public static void delete(int _idInscription){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE FROM Inscription WHERE idInscription=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idInscription);
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //suppression des inscription à une conférence en BDD
    public static void deleteInscriptionConf(int _idConference){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE FROM Inscription WHERE idConference=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idConference);
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //modification d'une inscription en BDD
    public static void update(Inscription _inscription){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Inscription set idSalarie=?,idConference=? "
                + "WHERE idInscription=?";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _inscription.getSalarie().getIdSalarie());
            ps.setInt(2, _inscription.getConference().getIdConference());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Theme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class ThemeDAO {
    //récupération de la liste des conférenciers
    public static ArrayList<Theme> getAll(){
        ArrayList<Theme> maListeThemes = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Theme ORDER BY designationTheme ASC";
        ResultSet resultat=exec(query);
        try{
            while (resultat.next()){
                int idTheme=resultat.getInt("idTheme");
                String designationTheme=resultat.getString("designationTheme");
                Theme theme=new Theme(idTheme,designationTheme);
                maListeThemes.add(theme);
            }
        } catch(SQLException ex){
            Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
        return maListeThemes;
    }
    
    //récupération d'un conférencier de la BDD
    public static Theme get(int _idTheme){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Theme theme=null;
        String query="SELECT * FROM Theme WHERE idTheme=?";
        try {
            ps=_c.prepareCall(query);
            ps.setInt(1, _idTheme);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idTheme=resultat.getInt("idTheme");
                String designationTheme=resultat.getString("designationTheme");
                theme= new Theme(idTheme, designationTheme);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theme;
    }
    
    //ajout d'un conférencier dans la BDD
    public static void insert(Theme _theme){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Theme"
                + "(designationTheme)"
                + "VALUES (?)";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _theme.getDesignationTheme());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre thème a bien été ajouté","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE, null, ex);
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'ajout de votre thème","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //suppression d'un conférencier en BDD
    public static void delete(int _idTheme){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE * FROM Theme WHERE idTheme=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idTheme);
            ps.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //modification d'un conférencier en BDD
    public static void update(Theme _theme){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Theme SET designationTheme=? "
                + "WHERE idTheme=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _theme.getDesignationTheme());
            ps.setInt(2, _theme.getIdTheme());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import fr.csi.Database.POJO.Salarie;
import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class SalarieDAO {
    //récupération de la liste des conférenciers
    public static ArrayList<Salarie> getAll(){
        ArrayList<Salarie> maListeSalaries = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Salarie";
        ResultSet resultat=exec(query);
        try{
            while (resultat.next()){
                int idSalarie=resultat.getInt("idSalarie");
                String nomPrenomSalarie=resultat.getString("nomPrenomSalarie");
                Salarie salarie=new Salarie(idSalarie,nomPrenomSalarie);
                maListeSalaries.add(salarie);
            }
        } catch(SQLException ex){
            Logger.getLogger(SalarieDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
        return maListeSalaries;
    }
    
    //récupération d'un conférencier de la BDD
    public static Salarie get(int _idSalarie){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Salarie salarie=null;
        String query="SELECT * FROM Salarie WHERE idSalarie=?";
        try {
            ps=_c.prepareCall(query);
            ps.setInt(1, _idSalarie);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idSalarie=resultat.getInt("idSalarie");
                String nomPrenomSalarie=resultat.getString("nomPrenomSalarie");
                salarie= new Salarie(idSalarie, nomPrenomSalarie);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalarieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salarie;
    }
    
    //ajout d'un conférencier dans la BDD
    public static void insert(Salarie _salarie){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Salarie"
                + "(nomPrenomSalarie)"
                + "VALUES (?)";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _salarie.getNomPrenomSalarie());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre salarié a bien été ajouté","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(SalarieDAO.class.getName()).log(Level.SEVERE, null, ex);
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'ajout de votre salarié","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //suppression d'un conférencier en BDD
    public static void delete(int _idSalarie){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE * FROM Salarie WHERE idSalarie=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idSalarie);
            ps.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(SalarieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //modification d'un conférencier en BDD
    public static void update(Salarie _salarie){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Salarie SET nomPrenomSalarie=? "
                + "WHERE idSalarie=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _salarie.getNomPrenomSalarie());
            ps.setInt(2, _salarie.getIdSalarie());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(SalarieDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

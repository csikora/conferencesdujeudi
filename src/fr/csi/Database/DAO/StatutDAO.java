/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Statut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class StatutDAO {
    //récupération de la liste des conférenciers
    public static ArrayList<Statut> getAll(){
        ArrayList<Statut> maListeStatuts = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Statut";
        ResultSet resultat=exec(query);
        try{
            while (resultat.next()){
                int idStatut=resultat.getInt("idStatut");
                String designationStatut=resultat.getString("designationStatut");
                Statut statut=new Statut(idStatut,designationStatut);
                maListeStatuts.add(statut);
            }
        } catch(SQLException ex){
            Logger.getLogger(StatutDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
        return maListeStatuts;
    }
    
    //récupération d'un conférencier de la BDD
    public static Statut get(int _idStatut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Statut statut=null;
        String query="SELECT * FROM Statut WHERE idStatut=?";
        try {
            ps=_c.prepareCall(query);
            ps.setInt(1, _idStatut);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idStatut=resultat.getInt("idStatut");
                String designationStatut=resultat.getString("designationStatut");
                statut= new Statut(idStatut, designationStatut);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return statut;
    }
    
    //ajout d'un conférencier dans la BDD
    public static void insert(Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Statut"
                + "(designationStatut)"
                + "VALUES (?)";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _statut.getDesignationStatut());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(StatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //suppression d'un conférencier en BDD
    public static void delete(int _idStatut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE * FROM Statut WHERE idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idStatut);
            ps.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(StatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //modification d'un conférencier en BDD
    public static void update(Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Statut SET designationStatut=? "
                + "WHERE idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _statut.getDesignationStatut());
            ps.setInt(2, _statut.getIdStatut());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(StatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

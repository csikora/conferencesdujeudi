/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Inscription;
import fr.csi.Database.POJO.InscriptionStatut;
import fr.csi.Database.POJO.Statut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class InscriptionStatutDAO {
    //récupération de la liste des conférencesStatut de la BDD
    public static ArrayList<InscriptionStatut> getAll(){
        ArrayList<InscriptionStatut> maListeInscriptionStatuts = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Inscriptionstatut";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                Inscription inscription=InscriptionDAO.get(resultat.getInt("idInscription"));
                Statut statut=StatutDAO.get(resultat.getInt("idStatut"));
                Calendar dateInscriptionStatut=Calendar.getInstance();
                dateInscriptionStatut.setTimeInMillis((resultat.getDate("dateInscriptionstatut")).getTime());
                InscriptionStatut inscriptionStatut = new InscriptionStatut(inscription,statut, dateInscriptionStatut);
                maListeInscriptionStatuts.add(inscriptionStatut);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeInscriptionStatuts;
    }
    
    //récupération d'une conférenceStatut de la BDD
    public static InscriptionStatut get(Inscription _inscription,Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        InscriptionStatut inscriptionStatut = null;
        String query="SELECT * FROM Inscription WHERE idInscription=? AND idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _inscription.getIdInscription());
            ps.setInt(2, _statut.getIdStatut());
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                Inscription inscription=InscriptionDAO.get(resultat.getInt("idInscription"));
                Statut statut=StatutDAO.get(resultat.getInt("idStatut"));
                Calendar dateInscriptionStatut=Calendar.getInstance();
                dateInscriptionStatut.setTimeInMillis((resultat.getDate("dateInscriptionstatut")).getTime());
                inscriptionStatut = new InscriptionStatut(inscription,statut, dateInscriptionStatut);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InscriptionStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inscriptionStatut;
    }
    
    //ajout d'une conférenceStatut en BDD
    public static void insert(int _idStatut){
        JOptionPane jop;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Inscriptionstatut"
                + "(idInscription,idStatut,dateStatutInscription)"
                + "VALUES ((SELECT Max(IdInscription) FROM inscription),?,CURRENT_DATE)";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idStatut);
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
            jop=new JOptionPane();
            jop.showMessageDialog(null,"Une erreur s'est produite lors de l'inscription","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //supprission d'une conférenceStatut en BDD
    public static void delete(Inscription _inscription,Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE FROM Inscription WHERE idInscription=? AND idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _inscription.getIdInscription());
            ps.setInt(2, _statut.getIdStatut());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //modification d'une conférenceStatut en BDD
    public static void update(InscriptionStatut _inscriptionStatut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Inscriptionstatut set idInscription=?,"
                + "idStatut=?,dateInscription=? "
                + "WHERE idInscription=? AND idStatut=?";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _inscriptionStatut.getInscription().getIdInscription());
            ps.setInt(2, _inscriptionStatut.getStatut().getIdStatut());
            ps.setDate(3, _inscriptionStatut.getSqlDate());
            ps.setInt(4, _inscriptionStatut.getInscription().getIdInscription());
            ps.setInt(5, _inscriptionStatut.getStatut().getIdStatut());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
}

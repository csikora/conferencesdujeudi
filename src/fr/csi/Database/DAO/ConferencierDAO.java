/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Conferencier;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class ConferencierDAO {
    //récupération de la liste des conférenciers
    public static ArrayList<Conferencier> getAll(){
        ArrayList<Conferencier> maListeConferenciers = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conferencier ORDER BY nomPrenomConferencier ASC";
        ResultSet resultat=exec(query);
        try{
            while (resultat.next()){
                int idConferencier=resultat.getInt("idConferencier");
                String nomPrenomConferencier=resultat.getString("nomPrenomConferencier");
                boolean conferencierInterne=resultat.getBoolean("conferencierInterne");
                String blocNoteConferencier=resultat.getString("blocNoteConferencier");
                Conferencier conferencier=new Conferencier(idConferencier,nomPrenomConferencier,conferencierInterne,blocNoteConferencier);
                maListeConferenciers.add(conferencier);
            }
        } catch(SQLException ex){
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
        return maListeConferenciers;
    }
    
    //récupération d'un conférencier de la BDD
    public static Conferencier get(int _idConferencier){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Conferencier conferencier=null;
        String query="SELECT * FROM Conferencier WHERE idConferencier=?";
        try {
            ps=_c.prepareCall(query);
            ps.setInt(1, _idConferencier);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idConferencier=resultat.getInt("idConferencier");
                String nomPrenomConferencier=resultat.getString("nomPrenomConferencier");
                boolean conferencierInterne=resultat.getBoolean("conferencierInterne");
                String blocNoteConferencier=resultat.getString("blocNoteConferencier");
                conferencier= new Conferencier(idConferencier,nomPrenomConferencier,conferencierInterne,blocNoteConferencier);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conferencier;
    }
    
    //ajout d'un conférencier dans la BDD
    public static void insert(Conferencier _conferencier){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Conferencier"
                + "(nomPrenomConferencier,conferencierInterne,blocNoteConferencier)"
                + "VALUES (?,?,?)";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _conferencier.getNomPrenomConferencier());
            ps.setBoolean(2, _conferencier.isConferencierInterne());
            ps.setString(3, _conferencier.getBlocNoteConferencier());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre conférencier a bien été ajoutée","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'ajout de votre conférencier","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //suppression d'un conférencier en BDD
    public static void delete(int _idConferencier){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE * FROM Conferencier WHERE idConferencier=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idConferencier);
            ps.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //modification d'un conférencier en BDD
    public static void update(Conferencier _conferencier){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Conferencier SET nomPrenomConferencier=?,"
                + "conferencierInterne=?,blocNoteConferencier=?"
                + "WHERE idConferencier=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _conferencier.getNomPrenomConferencier());
            ps.setBoolean(2, _conferencier.isConferencierInterne());
            ps.setString(3, _conferencier.getBlocNoteConferencier());
            ps.setInt(4, _conferencier.getIdConferencier());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

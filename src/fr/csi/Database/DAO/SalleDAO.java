/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Conferencier;
import fr.csi.Database.POJO.Salle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class SalleDAO {
   //récupération de la liste des salles
    public static ArrayList<Salle> getAll(){
        ArrayList<Salle> maListeSalles = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Salle ORDER BY nomSalle ASC";
        ResultSet resultat=exec(query);
        try{
            while (resultat.next()){
                int idSalle=resultat.getInt("idSalle");
                String nomSalle=resultat.getString("nomSalle");
                int nbPlaceSalle=resultat.getInt("nbPlaceSalle");
                Salle salle=new Salle(idSalle,nomSalle,nbPlaceSalle);
                maListeSalles.add(salle);
            }
        } catch(SQLException ex){
            Logger.getLogger(SalleDAO.class.getName()).log(Level.SEVERE,null,ex);
        }
        return maListeSalles;
    }
    
    //récupération d'un conférencier de la BDD
    public static Salle get(int _idSalle){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Salle salle=null;
        String query="SELECT * FROM Salle WHERE idSalle=?";
        try {
            ps=_c.prepareCall(query);
            ps.setInt(1, _idSalle);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idSalle=resultat.getInt("idSalle");
                String nomSalle=resultat.getString("nomSalle");
                int nbPlaceSalle=resultat.getInt("nbPlaceSalle");
                salle=new Salle(idSalle,nomSalle,nbPlaceSalle);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salle;
    }
    
    //ajout d'un conférencier dans la BDD
    public static void insert(Salle _salle){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Salle"
                + "(nomSalle,nbPlaceSalle)"
                + "VALUES (?,?)";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _salle.getNomSalle());
            ps.setInt(2, _salle.getNbPlaceSalle());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre salle a bien été ajoutée","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(SalleDAO.class.getName()).log(Level.SEVERE, null, ex);
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'ajout de votre salle","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //suppression d'un conférencier en BDD
    public static void delete(int _idSalle){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE * FROM Salle WHERE idSalle=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idSalle);
            ps.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(SalleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //modification d'un conférencier en BDD
    public static void update(Salle _salle){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Salle SET nomSalle=?,nbPLcaSalle=? "
                + "WHERE idSalle=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _salle.getNomSalle());
            ps.setInt(2, _salle.getNbPlaceSalle());
            ps.setInt(3, _salle.getIdSalle());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(SalleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.Conferencier;
import fr.csi.Database.POJO.Salle;
import fr.csi.Database.POJO.Theme;
import fr.csi.Panels.Home;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class ConferenceDAO {
    
    
    //récupération de la liste des conférences de la BDD
    public static ArrayList<Conference> getAll(){
        ArrayList<Conference> maListeConferences = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conference";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                int idConference=resultat.getInt("idConference");
                String titreConference=resultat.getString("titreConference");
                Calendar dateConference=Calendar.getInstance();
                dateConference.setTimeInMillis((resultat.getDate("dateConference")).getTime());
                Conferencier conferencier=ConferencierDAO.get(resultat.getInt("idConferencier"));
                Salle salle=SalleDAO.get(resultat.getInt("idSalle"));
                Theme theme=ThemeDAO.get(resultat.getInt("idTheme"));
                Conference conference = new Conference(idConference,titreConference, dateConference, conferencier,salle, theme);
                maListeConferences.add(conference);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeConferences;
    }
    
    //récupération de la liste des conférences répondant à la recherche
    public static ArrayList<Conference> getSearch(String _recherche){
        _recherche="%"+_recherche+"%";
        PreparedStatement ps=null;
        ArrayList<Conference> maListeConferences = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conference WHERE titreConference LIKE ?";
        try {
            ps=_c.prepareStatement(query);
            ps.setString(1, _recherche);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idConference=resultat.getInt("idConference");
                String titreConference=resultat.getString("titreConference");
                Calendar dateConference=Calendar.getInstance();
                dateConference.setTimeInMillis((resultat.getDate("dateConference")).getTime());
                Conferencier conferencier=ConferencierDAO.get(resultat.getInt("idConferencier"));
                Salle salle=SalleDAO.get(resultat.getInt("idSalle"));
                Theme theme=ThemeDAO.get(resultat.getInt("idTheme"));
                Conference conference = new Conference(idConference,titreConference, dateConference, conferencier,salle, theme);
                maListeConferences.add(conference);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeConferences;
    }
    
    //récupération d'une conférence de la BDD
    public static Conference get(int _idConference){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        Conference conference = null;
        String query="SELECT * FROM Conference WHERE idConference=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idConference);
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                int idConference=resultat.getInt("idConference");
                String titreConference=resultat.getString("titreConference");
                Calendar dateConference=Calendar.getInstance();
                dateConference.setTimeInMillis((resultat.getDate("dateConference")).getTime());
                Conferencier conferencier=ConferencierDAO.get(resultat.getInt("idConferencier"));
                Salle salle=SalleDAO.get(resultat.getInt("idSalle"));
                Theme theme=ThemeDAO.get(resultat.getInt("idTheme"));
                conference = new Conference(idConference,titreConference, dateConference, conferencier,salle, theme);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conference;
    }
    
    //ajout d'une conférence en BDD
    public static void insert(Conference _conference){
        JOptionPane jop1,jop2;
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Conference"
                + "(titreConference,dateConference,idConferencier,idSalle,idTheme)"
                + "VALUES (?,?,?,?,?)";
        try{
            ps=_c.prepareStatement(query);
            ps.setString(1, _conference.getTitreConference());
            ps.setDate(2, _conference.getSqlDate());
            ps.setInt(3, _conference.getConferencier().getIdConferencier());
            ps.setInt(4, _conference.getSalle().getIdSalle());
            ps.setInt(5, _conference.getTheme().getIdTheme());
            ps.execute();
            jop1=new JOptionPane();
            jop1.showMessageDialog(null,"Votre conférence a bien été ajoutée","Information",JOptionPane.INFORMATION_MESSAGE);
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
            jop2=new JOptionPane();
            jop2.showMessageDialog(null,"Une erreur s'est produite lors de l'ajout de votre conférence","Erreur",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //supprission d'une conférence en BDD
    public static void delete(int _idConference){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE FROM Conference WHERE idConference=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _idConference);
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //modification d'une conférence en BDD
    public static void update(Conference _conference){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Conference set titreConference=?,dateConference=?,"
                + "idConferencier=?,idSalle=?,idTheme=?"
                + "WHERE idConference=?";
        try{
            ps=_c.prepareStatement(query);
            ps.setString(1, _conference.getTitreConference());
            ps.setDate(2, _conference.getSqlDate());
            ps.setInt(3, _conference.getConferencier().getIdConferencier());
            ps.setInt(4, _conference.getSalle().getIdSalle());
            ps.setInt(5, _conference.getTheme().getIdTheme());
            ps.setInt(6, _conference.getIdConference());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    public static ArrayList<Conference> getAllOld(){
        ArrayList<Conference> maListeConferences = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conference WHERE dateConference<CURRENT_DATE";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                int idConference=resultat.getInt("idConference");
                String titreConference=resultat.getString("titreConference");
                Calendar dateConference=Calendar.getInstance();
                dateConference.setTimeInMillis((resultat.getDate("dateConference")).getTime());
                Conferencier conferencier=ConferencierDAO.get(resultat.getInt("idConferencier"));
                Salle salle=SalleDAO.get(resultat.getInt("idSalle"));
                Theme theme=ThemeDAO.get(resultat.getInt("idTheme"));
                Conference conference = new Conference(idConference,titreConference, dateConference, conferencier,salle, theme);
                maListeConferences.add(conference);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeConferences;
    }
    
    public static ArrayList<Conference> getAllNew(){
        ArrayList<Conference> maListeConferences = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conference WHERE dateConference>=CURRENT_DATE";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                int idConference=resultat.getInt("idConference");
                String titreConference=resultat.getString("titreConference");
                Calendar dateConference=Calendar.getInstance();
                dateConference.setTimeInMillis((resultat.getDate("dateConference")).getTime());
                Conferencier conferencier=ConferencierDAO.get(resultat.getInt("idConferencier"));
                Salle salle=SalleDAO.get(resultat.getInt("idSalle"));
                Theme theme=ThemeDAO.get(resultat.getInt("idTheme"));
                Conference conference = new Conference(idConference,titreConference, dateConference, conferencier,salle, theme);
                maListeConferences.add(conference);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeConferences;
    }
    
}

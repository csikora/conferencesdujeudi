/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.DAO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.exec;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import fr.csi.Database.POJO.Conference;
import fr.csi.Database.POJO.ConferenceStatut;
import fr.csi.Database.POJO.Statut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ConferenceStatutDAO {
    //récupération de la liste des conférencesStatut de la BDD
    public static ArrayList<ConferenceStatut> getAll(){
        ArrayList<ConferenceStatut> maListeConferenceStatuts = new ArrayList<>();
        Connection _c=getConnexion();
        String query="SELECT * FROM Conferencestatut";
        ResultSet resultat=exec(query);
        try {
            while (resultat.next()){
                Conference conference=ConferenceDAO.get(resultat.getInt("idConference"));
                Statut statut=StatutDAO.get(resultat.getInt("idStatut"));
                Calendar dateConferenceStatut=Calendar.getInstance();
                dateConferenceStatut.setTimeInMillis((resultat.getDate("dateConferencestatut")).getTime());
                ConferenceStatut conferenceStatut = new ConferenceStatut(conference,statut, dateConferenceStatut);
                maListeConferenceStatuts.add(conferenceStatut);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maListeConferenceStatuts;
    }
    
    //récupération d'une conférenceStatut de la BDD
    public static ConferenceStatut get(Conference _conference,Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        ConferenceStatut conferenceStatut = null;
        String query="SELECT * FROM Conference WHERE idConference=? AND idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _conference.getIdConference());
            ps.setInt(2, _statut.getIdStatut());
            ResultSet resultat=ps.executeQuery();
            while (resultat.next()){
                Conference conference=ConferenceDAO.get(resultat.getInt("idConference"));
                Statut statut=StatutDAO.get(resultat.getInt("idStatut"));
                Calendar dateConferenceStatut=Calendar.getInstance();
                dateConferenceStatut.setTimeInMillis((resultat.getDate("dateConferencestatut")).getTime());
                conferenceStatut = new ConferenceStatut(conference,statut, dateConferenceStatut);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conferenceStatut;
    }
    
    //ajout d'une conférenceStatut en BDD
    public static void insert(ConferenceStatut _conferenceStatut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="INSERT INTO Conferencestatut"
                + "(idConference,idStatut,dateConferenceStatut)"
                + "VALUES (?,?,?)";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _conferenceStatut.getConference().getIdConference());
            ps.setInt(2, _conferenceStatut.getStatut().getIdStatut());
            ps.setDate(3, _conferenceStatut.getSqlDate());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //supprission d'une conférenceStatut en BDD
    public static void delete(Conference _conference,Statut _statut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="DELETE FROM Conference WHERE idConference=? AND idStatut=?";
        try {
            ps=_c.prepareStatement(query);
            ps.setInt(1, _conference.getIdConference());
            ps.setInt(2, _statut.getIdStatut());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
    
    //modification d'une conférenceStatut en BDD
    public static void update(ConferenceStatut _conferenceStatut){
        PreparedStatement ps=null;
        Connection _c=getConnexion();
        String query="UPDATE Conferencestatut set idConference=?,"
                + "idStatut=?,dateConference=? "
                + "WHERE idConference=? AND idStatut=?";
        try{
            ps=_c.prepareStatement(query);
            ps.setInt(1, _conferenceStatut.getConference().getIdConference());
            ps.setInt(2, _conferenceStatut.getStatut().getIdStatut());
            ps.setDate(3, _conferenceStatut.getSqlDate());
            ps.setInt(4, _conferenceStatut.getConference().getIdConference());
            ps.setInt(5, _conferenceStatut.getStatut().getIdStatut());
            ps.execute();
        } catch(SQLException ex){
            System.err.println(ex.getMessage());
        }
    }
}

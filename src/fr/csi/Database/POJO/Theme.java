/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

/**
 *
 * @author Asus
 */
public class Theme {
    
    //les attributs
    private int idTheme;
    private String designationTheme;
    
    //le constructeur
    public Theme(int idTheme, String designationTheme) {
        this.idTheme = idTheme;
        this.designationTheme = designationTheme;
    }
    public Theme(String designationTheme) {
        this.designationTheme = designationTheme;
    }
    
    //les méthodes
    public int getIdTheme() {
        return idTheme;
    }
    public void setIdTheme(int idTheme) {
        this.idTheme = idTheme;
    }
    public String getDesignationTheme() {
        return designationTheme;
    }
    public void setDesignationTheme(String designationTheme) {
        this.designationTheme = designationTheme;
    }
    @Override
    public String toString(){
        return this.designationTheme;
    }
}

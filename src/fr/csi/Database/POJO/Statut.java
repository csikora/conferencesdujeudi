/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

/**
 *
 * @author Asus
 */
public class Statut {
    
    //les attributs
    private int idStatut;
    private String designationStatut;
    
    //le constructeur
    public Statut(int idStatut, String designationStatut) {
        this.idStatut = idStatut;
        this.designationStatut = designationStatut;
    }
    
    //les méthodes
    public int getIdStatut() {
        return idStatut;
    }
    public void setIdStatut(int idStatut) {
        this.idStatut = idStatut;
    }
    public String getDesignationStatut() {
        return designationStatut;
    }
    public void setDesignationStatut(String designationStatut) {
        this.designationStatut = designationStatut;
    }
    @Override
    public String toString(){
        return this.designationStatut;
    }
    
}

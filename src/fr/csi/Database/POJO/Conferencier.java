/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;



/**
 *
 * @author Asus
 */
public class Conferencier {
    
    //les attributs
    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private String blocNoteConferencier;
    
    //les constructeurs
    public Conferencier(){
        
    }
    
    public Conferencier(int _idConferencier,String _nomPrenomConferencier)
    {
        this.idConferencier=_idConferencier;
        this.nomPrenomConferencier=_nomPrenomConferencier;
    }   
    
    public Conferencier(int _idConferencier,String _nomPrenomConferencier,boolean _conferencierInterne,String _blocNoteConferencier){
        this.idConferencier=_idConferencier;
        this.nomPrenomConferencier=_nomPrenomConferencier;
        this.conferencierInterne=_conferencierInterne;
        this.blocNoteConferencier=_blocNoteConferencier;
    }
    
    public Conferencier(String _nomPrenomConferencier,boolean _conferencierInterne,String _blocNoteConferencier){
        this.nomPrenomConferencier=_nomPrenomConferencier;
        this.conferencierInterne=_conferencierInterne;
        this.blocNoteConferencier=_blocNoteConferencier;
    }
    
    //les méthodes
    public int getIdConferencier(){
        return this.idConferencier;
    }
    public void setIdConferencier(int _idConferencier){
        this.idConferencier=_idConferencier;
    }
    
    public String getNomPrenomConferencier(){
        return this.nomPrenomConferencier;
    }
    public void setNomPrenomConferencier(String _nomPrenomConferencier){
        this.nomPrenomConferencier=_nomPrenomConferencier;
    }
    
    public boolean isConferencierInterne(){
        return this.conferencierInterne;
    }
    public void setConferencierInterne(boolean _conferencierInterne){
        this.conferencierInterne=_conferencierInterne;
    }
        
    public String getBlocNoteConferencier(){
        return this.blocNoteConferencier;
    }
    public void setBlocNoteConferencier(String _blocNoteConferencier){
        this.blocNoteConferencier=_blocNoteConferencier;
    }
    @Override
    public String toString(){
        return this.nomPrenomConferencier;
    }
}

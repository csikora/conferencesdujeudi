/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

/**
 *
 * @author Asus
 */
public class Inscription {
    
    //les attributs
    private int idInscription;
    private Salarie salarie;
    private Conference conference;
    
    //le constructeur
    public Inscription(int _idInscription,Salarie _salarie,Conference _conference){
        this.idInscription=_idInscription;
        this.salarie=_salarie;
        this.conference=_conference;
    }
    
    //les méthodes
    public int getIdInscription(){
        return this.idInscription;
    }
    public void setIdInscription(int _idInscription){
        this.idInscription=_idInscription;
    }
    public Salarie getSalarie(){
        return this.salarie;
    }
    public void setSalarie(Salarie _salarie){
        this.salarie=_salarie;
    }
    public Conference getConference(){
        return this.conference;
    }
    public void setConference(Conference _conference){
        this.conference=_conference;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Asus
 */
public class InscriptionStatut {
    
    //les attributs
    private Inscription inscription;
    private Statut statut;
    private Calendar dateStatutInscription;
    //je rajoute une date au format java.sql.Date comme dans la classe Conference
    private java.sql.Date dateSQL;
    
    //le constructeur
    public InscriptionStatut(Inscription _inscription,Statut _statut,Calendar _dateStatutInscription){
        this.inscription=_inscription;
        this.statut=_statut;
        this.dateStatutInscription=_dateStatutInscription;
    }
    
    
    //les méthodes
    public Inscription getInscription(){
        return this.inscription;
    }
    public void setInscription(Inscription _inscription){
        this.inscription=_inscription;
    }
    public Statut getStatut(){
        return this.statut;
    }
    public void setStatut(Statut _statut){
        this.statut=_statut;                
    }
    public Calendar getDateSatutInscription(){
        return this.dateStatutInscription;
    }
    public void setDateStatutInscription(Calendar _dateStatutInscription){
        this.dateStatutInscription=_dateStatutInscription;
    }
    
    /**
     * Je rajoute les méthodes qui ont été rajoutées dans la classe Conference
     * getDateString() pour retourner la date au format texte
     * getSqlDate() pour passer la date du format Calendar au format java.sql.Date
     */
    public String getDateString(){
        SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateSatutInscription().getTimeInMillis());
    }
    public java.sql.Date getSqlDate(){
        dateSQL=new java.sql.Date(this.dateStatutInscription.getTime().getTime());
        return dateSQL;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Asus
 */
public class ConferenceStatut {
    
    //les attributs
    private Conference conference;
    private Statut statut;
    private Calendar dateStatutConference;
    //je rajoute une date au format java.sql.Date comme dans la classe Conference
    private java.sql.Date dateSQL;
    
    //le constructeur
    public ConferenceStatut(Conference _conference,Statut _statut,Calendar _dateStatutConference)
    {
        this.conference=_conference;
        this.statut=_statut;
        this.dateStatutConference=_dateStatutConference;
    } 
    
    //les méthodes
    public Conference getConference(){
        return this.conference;
    }
    public void setConference(Conference _conference){
        this.conference=_conference;
    }
    
    public Statut getStatut(){
        return this.statut;
    }
    public void setStatut(Statut _statut){
        this.statut=_statut;
    }
    
    public Calendar getDateStatutConference(){
        return this.dateStatutConference;
    }
    public void setDateStatutConference(Calendar _dateStatutConference){
        this.dateStatutConference=_dateStatutConference;
    }
    
    /**
     * Je rajoute les méthodes qui ont été rajoutées dans la classe Conference
     * getDateString() pour retourner la date au format texte
     * getSqlDate() pour passer la date du format Calendar au format java.sql.Date
     */
    public String getDateString(){
        SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateStatutConference().getTimeInMillis());
    }
    public java.sql.Date getSqlDate(){
        dateSQL=new java.sql.Date(this.dateStatutConference.getTime().getTime());
        return dateSQL;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

import com.mysql.jdbc.Connection;
import static fr.csi.Database.DatabaseUtiles.getConnexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Asus
 */
public class Conference {

    //les attributs
    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private Conferencier conferencier;
    private Salle salle;
    private Theme theme;
    private java.sql.Date dateSQL;

    //les constructeurs
    public Conference() {

    }

    public Conference(int _idConference, String _titreConference, Conferencier _conferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.conferencier = _conferencier;
    }

    public Conference(int _idConference, String _titreConference, Calendar _dateConference, Conferencier _conferencier, Salle _salle, Theme _theme) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.conferencier = _conferencier;
        this.salle = _salle;
        this.theme = _theme;
    }

    public Conference(String _titreConference, Calendar _dateConference, Conferencier _conferencier, Salle _salle, Theme _theme) {
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.conferencier = _conferencier;
        this.salle = _salle;
        this.theme = _theme;
    }

    //les méthodes
    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }

    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public Calendar getDateConference() {
        return this.dateConference;
    }

    public void setDateConference(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(this.getDateConference().getTimeInMillis());
    }

    public Conferencier getConferencier() {
        return this.conferencier;
    }

    public void setConferencier(Conferencier _conferencier) {
        this.conferencier = _conferencier;
    }

    public Salle getSalle() {
        return this.salle;
    }

    public void setSalle(Salle _salle) {
        this.salle = _salle;
    }

    public Theme getTheme() {
        return this.theme;
    }

    public void setTheme(Theme _theme) {
        this.theme = _theme;
    }

    public java.sql.Date getSqlDate() {
        dateSQL = new java.sql.Date(this.dateConference.getTime().getTime());
        return dateSQL;
    }

    @Override
    public String toString() {
        return this.titreConference;
    }
}

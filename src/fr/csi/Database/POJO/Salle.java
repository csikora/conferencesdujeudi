/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

/**
 *
 * @author Asus
 */
public class Salle {
    
    //les attributs
    private int idSalle;
    private String nomSalle;
    private int nbPlaceSalle;
    
    //le constructeur
    public Salle(int idSalle, String nomSalle, int nbPlaceSalle) {
        this.idSalle = idSalle;
        this.nomSalle = nomSalle;
        this.nbPlaceSalle = nbPlaceSalle;
    }

    
    public Salle(String nomSalle, int nbPlaceSalle) {
        this.nomSalle = nomSalle;
        this.nbPlaceSalle = nbPlaceSalle;
    }
    
    //les méthodes

    public int getIdSalle() {
        return idSalle;
    }
    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }
    public String getNomSalle() {
        return nomSalle;
    }
    public void setNomSalle(String nomSalle) {
        this.nomSalle = nomSalle;
    }
    public int getNbPlaceSalle() {
        return nbPlaceSalle;
    }
    public void setNbPlaceSalle(int nbPlaceSalle) {
        this.nbPlaceSalle = nbPlaceSalle;
    }
    @Override
    public String toString(){
        return this.nomSalle;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database.POJO;

/**
 *
 * @author Asus
 */
public class Salarie {
    
    //les attributs
    private int idSalarie;
    private String nomPrenomSalarie;
    
    //le constructeur
    public Salarie(int idSalarie, String nomPrenomSalarie) {
        this.idSalarie = idSalarie;
        this.nomPrenomSalarie = nomPrenomSalarie;
    }
    
    public Salarie(String nomPrenomSalarie) {
        this.nomPrenomSalarie = nomPrenomSalarie;
    }
    
    //les méthodes
    public int getIdSalarie() {
        return idSalarie;
    }
    public void setIdSalarie(int idSalarie) {
        this.idSalarie = idSalarie;
    }
    public String getNomPrenomSalarie() {
        return nomPrenomSalarie;
    }
    public void setNomPrenomSalarie(String nomPrenomSalarie) {
        this.nomPrenomSalarie = nomPrenomSalarie;
    }
    @Override
    public String toString(){
        return this.nomPrenomSalarie;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.csi.Database;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class DatabaseUtiles {

    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        Connection connection = null;

        try {
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/conferencesdujeudi", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    public static ResultSet exec(String _query) {
        Connection _c = getConnexion();
        Statement stmt = null;
        ResultSet resultat = null;
        try {
            stmt = _c.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtiles.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            resultat = stmt.executeQuery(_query);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtiles.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultat;
    }
    
        /*Utilisé avant la mise en place du MVC
    public static String[] getNomColonnes(ResultSet resultat){
        ResultSetMetaData metadata = null;
        try {
            metadata = resultat.getMetaData();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtiles.class.getName()).log(Level.SEVERE, null, ex);
        }
        String[] noms = null;
        try {
            noms = new String[metadata.getColumnCount()];
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtiles.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < noms.length; i++) {
            String nomColonne = null;
            try {
                nomColonne = metadata.getColumnName(i + 1);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseUtiles.class.getName()).log(Level.SEVERE, null, ex);
            }
            noms[i] = nomColonne;
        }
        return noms;
    }*/
}

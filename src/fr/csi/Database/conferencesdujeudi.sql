-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 21 mai 2019 à 15:06
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `conferencesdujeudi`
--

-- --------------------------------------------------------

--
-- Structure de la table `conference`
--

DROP TABLE IF EXISTS `conference`;
CREATE TABLE IF NOT EXISTS `conference` (
  `idConference` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Conference',
  `titreConference` varchar(50) NOT NULL COMMENT 'Titre de la Conference',
  `dateConference` date NOT NULL COMMENT 'Date de la Conference',
  `idConferencier` int(10) NOT NULL COMMENT 'Identifiant Conferencier',
  `idSalle` int(10) NOT NULL COMMENT 'Identifiant de la Salle',
  `idTheme` int(10) NOT NULL COMMENT 'Identifiant du Theme',
  PRIMARY KEY (`idConference`),
  KEY `fk_Conferencier_Conference` (`idConferencier`),
  KEY `fk_Salle_Conference` (`idSalle`),
  KEY `fk_Theme_Conference` (`idTheme`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `conference`
--

INSERT INTO `conference` (`idConference`, `titreConference`, `dateConference`, `idConferencier`, `idSalle`, `idTheme`) VALUES
(1, 'Les méthodes Agiles', '2019-05-09', 1, 1, 2),
(2, 'Les bases de l\'informatique', '2019-05-16', 2, 2, 4),
(3, 'La programmation Objet', '2019-05-21', 4, 1, 3),
(4, 'Les formulaires', '2019-05-23', 4, 1, 3),
(8, 'test1365656+6', '2019-01-01', 1, 1, 4),
(10, 'mon titre', '2018-10-12', 1, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `conferencestatut`
--

DROP TABLE IF EXISTS `conferencestatut`;
CREATE TABLE IF NOT EXISTS `conferencestatut` (
  `idConference` int(10) NOT NULL COMMENT 'Identifiant Conference',
  `idStatut` int(10) NOT NULL COMMENT 'Identifiant Statut',
  `dateStatutConference` date NOT NULL COMMENT 'Date du Statut de la Conference',
  KEY `fk_Conference_ConferenceStatut` (`idConference`),
  KEY `fk_Statut_ConferenceStatut` (`idStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `conferencier`
--

DROP TABLE IF EXISTS `conferencier`;
CREATE TABLE IF NOT EXISTS `conferencier` (
  `idConferencier` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Conferencier',
  `nomPrenomConferencier` varchar(50) NOT NULL COMMENT 'Nom et Prenom du Conferencier',
  `conferencierInterne` tinyint(1) DEFAULT NULL COMMENT 'Conferencier Interne ?',
  `blocNoteConferencier` varchar(255) DEFAULT NULL COMMENT 'Bloc Note du Conferencier',
  PRIMARY KEY (`idConferencier`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `conferencier`
--

INSERT INTO `conferencier` (`idConferencier`, `nomPrenomConferencier`, `conferencierInterne`, `blocNoteConferencier`) VALUES
(1, 'BOUKHRIS Fadhel', 1, NULL),
(2, 'GINER Serge', 1, NULL),
(3, 'VOSGIENS Thierry', 1, NULL),
(4, 'DAGNIAU Marie', 0, NULL),
(5, '', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

DROP TABLE IF EXISTS `inscription`;
CREATE TABLE IF NOT EXISTS `inscription` (
  `idInscription` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Inscription',
  `idSalarie` int(10) NOT NULL COMMENT 'Identifiant Salarie',
  `idConference` int(10) NOT NULL COMMENT 'Identifiant Conference',
  PRIMARY KEY (`idInscription`),
  KEY `fk_Salarie_Inscription` (`idSalarie`),
  KEY `fk_Conference_Inscription` (`idConference`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`idInscription`, `idSalarie`, `idConference`) VALUES
(1, 1, 3),
(2, 1, 3),
(3, 1, 4),
(4, 1, 3),
(5, 1, 3),
(6, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `inscriptionstatut`
--

DROP TABLE IF EXISTS `inscriptionstatut`;
CREATE TABLE IF NOT EXISTS `inscriptionstatut` (
  `idInscription` int(10) NOT NULL COMMENT 'Identifiant Inscription',
  `idStatut` int(10) NOT NULL COMMENT 'Identifiant Statut',
  `dateStatutInscription` date NOT NULL COMMENT 'Date du Statut de l inscription',
  KEY `fk_Inscription_InscriptionStatut` (`idInscription`),
  KEY `fk_Statut_InscriptionStatut` (`idStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inscriptionstatut`
--

INSERT INTO `inscriptionstatut` (`idInscription`, `idStatut`, `dateStatutInscription`) VALUES
(6, 2, '2019-05-20');

-- --------------------------------------------------------

--
-- Structure de la table `salarie`
--

DROP TABLE IF EXISTS `salarie`;
CREATE TABLE IF NOT EXISTS `salarie` (
  `idSalarie` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Salarie',
  `nomPrenomSalarie` varchar(50) NOT NULL COMMENT 'Nom et Prenom du Salarie',
  PRIMARY KEY (`idSalarie`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salarie`
--

INSERT INTO `salarie` (`idSalarie`, `nomPrenomSalarie`) VALUES
(1, 'BOUKHRIS Fadhel');

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `idSalle` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Salle',
  `nomSalle` varchar(50) NOT NULL COMMENT 'Nom de la salle',
  `nbPlaceSalle` int(10) NOT NULL COMMENT 'Nombre de places de la Salle',
  PRIMARY KEY (`idSalle`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`idSalle`, `nomSalle`, `nbPlaceSalle`) VALUES
(1, 'Salle 1', 100),
(2, 'Salle 2', 200),
(3, 'Salle 3', 100),
(4, 'Salle 4', 200);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `idStatut` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Statut',
  `designationStatut` varchar(50) NOT NULL COMMENT 'Designation du Statut',
  PRIMARY KEY (`idStatut`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`idStatut`, `designationStatut`) VALUES
(1, 'close'),
(2, 'inscrit'),
(3, 'attente');

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE IF NOT EXISTS `theme` (
  `idTheme` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Identifiant Theme',
  `designationTheme` varchar(50) NOT NULL COMMENT 'Designation du Theme',
  PRIMARY KEY (`idTheme`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`idTheme`, `designationTheme`) VALUES
(1, 'Merise'),
(2, 'UML'),
(3, 'JAVA'),
(4, 'Algoritmes');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `conference`
--
ALTER TABLE `conference`
  ADD CONSTRAINT `fk_Conferencier_Conference` FOREIGN KEY (`idConferencier`) REFERENCES `conferencier` (`idConferencier`),
  ADD CONSTRAINT `fk_Salle_Conference` FOREIGN KEY (`idSalle`) REFERENCES `salle` (`idSalle`),
  ADD CONSTRAINT `fk_Theme_Conference` FOREIGN KEY (`idTheme`) REFERENCES `theme` (`idTheme`);

--
-- Contraintes pour la table `conferencestatut`
--
ALTER TABLE `conferencestatut`
  ADD CONSTRAINT `fk_Conference_ConferenceStatut` FOREIGN KEY (`idConference`) REFERENCES `conference` (`idConference`),
  ADD CONSTRAINT `fk_Statut_ConferenceStatut` FOREIGN KEY (`idStatut`) REFERENCES `statut` (`idStatut`);

--
-- Contraintes pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `fk_Conference_Inscription` FOREIGN KEY (`idConference`) REFERENCES `conference` (`idConference`),
  ADD CONSTRAINT `fk_Salarie_Inscription` FOREIGN KEY (`idSalarie`) REFERENCES `salarie` (`idSalarie`);

--
-- Contraintes pour la table `inscriptionstatut`
--
ALTER TABLE `inscriptionstatut`
  ADD CONSTRAINT `fk_Inscription_InscriptionStatut` FOREIGN KEY (`idInscription`) REFERENCES `inscription` (`idInscription`),
  ADD CONSTRAINT `fk_Statut_InscriptionStatut` FOREIGN KEY (`idStatut`) REFERENCES `statut` (`idStatut`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
